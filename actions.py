#!/bin/python3
from subprocess import call
from os import environ
from typing import List

from functional import seq

mongo_name = "ddosmongo"


def present_and_execute(command: str) -> bool:
    print(command + "\n")
    cmdargs = seq(command.split(" ")).filter_not(lambda x: x == "").to_list()
    return call(cmdargs) == 0


def present_and_execute_async(command: str) -> bool:
    return present_and_execute("bash/detached_runner.sh {}".format(command))


def run_forwarding_sflowtool(sockets_to_forward_to: List[str]) -> bool:
    sufix = seq(sockets_to_forward_to) \
        .map(lambda addr: "-f {}".format(addr)) \
        .reduce(lambda x, y: "{}  {}".format(x, y))

    return present_and_execute("bash/detached_runner.sh sflowtool {} ".format(sufix))


def run_pcap_sflow(sflow_port: int) -> bool:
    seconds_per_file = 60
    files = 30
    return \
        present_and_execute("sudo mkdir /dumps") and \
        present_and_execute("sudo touch /dumps ddos.pcap") and \
        present_and_execute_async(
            "sudo sflowtool -p {} -t | sudo dumpcap -P -i - -b duration:{} -b files:{} -w /dumps/ddos.pcap".format(
                sflow_port,
                seconds_per_file, files))


def run_sflow_collector(sflow_port: int, server_port: int) -> bool:
    return present_and_execute_async("./sflow-collector -p {} -l ".format(sflow_port, server_port))


def start_local_mongo() -> bool:
    command = "docker run -d --name={} -p 0.0.0.0:27017:27017 mongo".format(mongo_name)
    return present_and_execute(command)


def run_fastnetmon(sflow_port: int, external_network: str) -> bool:
    environ["EXTERNAL_NETWORK"] = external_network
    environ["DOCKER_NETWORK"] = "172.17.0.0/16"  # probably not needed, as there is port mapping to localhost
    environ["OVS_NETWORK"] = "192.168.5.0/24"
    environ["FASTNETMON_SLFOW_PORT"] = str(sflow_port)
    environ["ANALYSIS_MONGO_CONTAINER_NAME"] = mongo_name
    return present_and_execute("bash/fastnetmon.sh")


def kill_sflows():
    print("Killing sflowtools \n")
    present_and_execute("killall sflowtool")
    present_and_execute("killall sflow-collector")


def clean_envs() -> None:
    print("Cleaning envs \n")
    environ["EXTERNAL_NETWORK"] = ""
    environ["DOCKER_NETWORK"] = ""
    environ["OVS_NETWORK"] = ""
    environ["FASTNETMON_SFLOW_PORT"] = ""
    environ["ANALYSIS_MONGO_CONTAINER_NAME"] = ""
    environ["MONGO_TTL_MINUTES"] = ""


def clean_dockers():
    print("Cleaning containers\n")
    present_and_execute("docker stop fastnetmon {}".format("")) # mongo_name
    present_and_execute("docker rm fastnetmon {}".format("")) #mongo_name
    # Not shutting down mongo


def clean_files():
    print("Cleaning files")
    present_and_execute("sudo rm -rf /dumps")
    present_and_execute("rm -rf /var/log/fastnetmon_attacks")
    present_and_execute("rm /var/log/fastnetmon.log")
    present_and_execute("rm /notify.sh")
    present_and_execute("rm /etc/fastnetmon.conf")


def clean():
    kill_sflows()
    clean_dockers()
    clean_envs()
    clean_files()
    return True
