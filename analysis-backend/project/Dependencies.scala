import sbt._

//noinspection SpellCheckingInspection
object Dependencies {

  private val mongoVersion        = "0.15.0"
  private val configVersion       = "1.3.3"
  private val monixVersion        = "3.0.0-8084549"
  private val logbackVersion      = "1.2.3"
  private val slf4jVersion        = "1.7.25"
  private val scalaLoggingVersion = "3.7.2"
  private val http4sVersion       = "0.19.0"
  private val circleVersion       = "0.9.3"
  private val json4sVersion       = "3.6.2"
  private val streamzVersion      = "0.9"
  private val akkaHttpVersion     = "10.1.3"
  private val twitterUtilVersion  = "18.8.0"
  private val hasherVersion       = "1.2.0"
  private val jwtVersion          = "0.14.1"
  private val akkaKafkaVersion    = "0.18"
  private val kafkaClientVersion  = "1.0.0"
  private val mouseVersion        = "0.19"
  private val fs2Version          = "1.0.0"
  private val scalapbVersion      = "0.3.2"

  private val loggingDependencies = Seq(
    "ch.qos.logback"             % "logback-classic"  % logbackVersion,
    "ch.qos.logback"             % "logback-core"     % logbackVersion,
    "org.slf4j"                  % "jcl-over-slf4j"   % slf4jVersion,
    "org.slf4j"                  % "log4j-over-slf4j" % slf4jVersion,
    "org.slf4j"                  % "jul-to-slf4j"     % slf4jVersion,
    "com.typesafe.scala-logging" %% "scala-logging"   % scalaLoggingVersion
  )

  private val akkaDependencies = Seq(
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion
  )

  private val http4sDependencies = Seq(
    "org.http4s" %% "http4s-dsl"          % http4sVersion,
    "org.http4s" %% "http4s-blaze-core"   % http4sVersion,
    "org.http4s" %% "http4s-blaze-server" % http4sVersion,
    "org.http4s" %% "http4s-blaze-client" % http4sVersion,
    "org.http4s" %% "http4s-circe"        % http4sVersion
  )

  private val streamzDependencies = Seq(
    "com.github.krasserm" %% "streamz-converter" % streamzVersion
  )

  private val circleDependencies = Seq(
    "io.circe" %% "circe-generic" % circleVersion,
    "io.circe" %% "circe-literal" % circleVersion,
    "io.circe" %% "circe-java8"   % circleVersion,
    "io.circe" %% "circe-parser"  % circleVersion
  )

  private val json4sDependencies = Seq(
    "org.json4s" %% "json4s-native" % json4sVersion
  )

  private val mongoDependencies = Seq(
    "org.reactivemongo" %% "reactivemongo"            % mongoVersion,
    "org.reactivemongo" %% "reactivemongo-akkastream" % mongoVersion
  )

  private val monixDependencies = Seq("io.monix" %% "monix" % monixVersion)

  private val kafkaDependencies = Seq(
    "org.apache.kafka" % "kafka-clients" % kafkaClientVersion
  )

  private val akkaKafkaDependencies = Seq(
    "com.typesafe.akka"      %% "akka-stream-kafka" % akkaKafkaVersion,
    "com.trueaccord.scalapb" %% "scalapb-json4s"    % scalapbVersion
  )

  private val utilDependencies = Seq(
    "com.typesafe"    % "config"           % configVersion,
    "com.twitter"     %% "util-collection" % twitterUtilVersion,
    "com.roundeights" %% "hasher"          % hasherVersion,
    "org.typelevel"   % "mouse_2.12"       % mouseVersion
  )

  private val jwtDependencies = Seq(
    "com.pauldijou" %% "jwt-json4s-native" % jwtVersion
  )

  private val fs2Dependencies = Seq(
    "co.fs2" %% "fs2-core" % fs2Version
  )

  val all: Seq[ModuleID] =
    loggingDependencies ++
      mongoDependencies ++
      monixDependencies ++
      http4sDependencies ++
      streamzDependencies ++
      circleDependencies ++
      json4sDependencies ++
      jwtDependencies ++
      akkaDependencies ++
      kafkaDependencies ++
      akkaKafkaDependencies ++
      fs2Dependencies ++
      utilDependencies

  val resolvers: Seq[MavenRepository] =
    Seq("krasserm at bintray" at "http://dl.bintray.com/krasserm/maven")

}
