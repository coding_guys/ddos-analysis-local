import java.net.{InetAddress, InetSocketAddress}
import java.nio.channels.AsynchronousChannelGroup
import java.util.concurrent.Executors

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import cats.effect.{ExitCode, IO, IOApp}
import com.typesafe.config.ConfigFactory
import domain.attack.detection.AccessRateDetectionService
import domain.packet.{PacketSample, PacketStream}
import fs2._
import fs2.concurrent.Topic
import fs2.io.tcp.client
import infrastructure.mongo.detection.{MongoSegmentAccessRateRepository, MongoSegmentPopularityRepository}
import org.http4s.client.blaze.BlazeClientBuilder
import reactivemongo.api.FailoverStrategy.FactorFun
import reactivemongo.api.{FailoverStrategy, MongoConnection, MongoDriver}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

object TrainingMain extends IOApp {

  private val config          = ConfigFactory.load()
  private val executorService = Executors.newFixedThreadPool(8)

  private implicit val timeout: FiniteDuration      = 10 seconds
  private implicit val system: ActorSystem          = ActorSystem("System", config)
  private implicit val materializer: Materializer   = ActorMaterializer()
  private implicit val ag: AsynchronousChannelGroup = AsynchronousChannelGroup.withThreadPool(executorService)

  private val appConfig = ConfigValueProvider.provide(config)

  private val driver           = MongoDriver()
  private val mongoConfig      = appConfig.mongoConfig
  private val mongoUri         = s"mongodb://${mongoConfig.host}:${mongoConfig.port}"
  private val connection       = MongoConnection.parseURI(mongoUri).map(driver.connection)
  private val failoverStrategy = FailoverStrategy(initialDelay = 300 millis, retries = 20, delayFactor = FactorFun(1.5))
  private val packetsDb        = IO.fromFuture(IO { Future.fromTry(connection).flatMap(_.database(mongoConfig.packetsDb, failoverStrategy)) })

  private val socketHost    = "127.0.0.1"
  private val socketPort    = 9999
  private val socketAddress = new InetSocketAddress(InetAddress.getByName(socketHost), socketPort)

  override def run(args: List[String]): IO[ExitCode] = {
    val stream =
      for {
        httpClient <- BlazeClientBuilder[IO](implicitly[ExecutionContext])
                       .withResponseHeaderTimeout(3 seconds)
                       .withIdleTimeout(3 seconds)
                       .withRequestTimeout(3 seconds)
                       .stream
        packetDb <- Stream.eval(packetsDb)

        packetTopic <- Stream.eval(Topic[IO, Option[PacketSample]](None))

        trainingAccessRateRepo = new MongoSegmentAccessRateRepository(packetDb, "training_accessRates", appConfig.accessRateDetectionConfig)
        testAccessRateRepo     = new MongoSegmentAccessRateRepository(packetDb, "test_accessRates", appConfig.accessRateDetectionConfig)
        trainingPopularityRepo = new MongoSegmentPopularityRepository(packetDb, "training_popularities", appConfig.accessRateDetectionConfig)

        detectionService = new AccessRateDetectionService(appConfig.accessRateDetectionConfig,
                                                          trainingAccessRateRepo,
                                                          trainingPopularityRepo,
                                                          testAccessRateRepo)

        _ <- Stream.eval(
              IO.race(
                PacketStream(client[IO](socketAddress), httpClient, packetTopic, aggregateTime = appConfig.accessRateDetectionConfig.smallBucket),
                detectionService.prepareTrainingData(
                  packetTopic
                    .subscribe(Int.MaxValue)
                    .collect { case Some(packetSample) => packetSample }
                )
              )
            )
      } yield ()

    stream.compile.drain.map(_ => ExitCode.Success)
  }

}
