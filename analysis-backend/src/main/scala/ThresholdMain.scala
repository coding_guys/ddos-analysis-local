import java.net.{InetAddress, InetSocketAddress}
import java.nio.channels.AsynchronousChannelGroup
import java.time.ZonedDateTime
import java.util.concurrent.Executors

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import cats.effect.{ExitCode, IO, IOApp, Timer}
import cats.implicits._
import com.typesafe.config.ConfigFactory
import domain.attack.detection.AccessRateDetectionService
import domain.attack.detection.scores.BadnessScores
import domain.packet.{PacketSample, PacketStream}
import fs2._
import fs2.concurrent.Topic
import fs2.io.tcp.client
import infrastructure.mongo.detection.score.{DetectionScoreRepository, DetectionScoreThresholdRepository}
import infrastructure.mongo.detection.{MongoSegmentAccessRateRepository, MongoSegmentPopularityRepository}
import infrastructure.util.StreamUtils
import org.http4s.client.blaze.BlazeClientBuilder
import reactivemongo.api.FailoverStrategy.FactorFun
import reactivemongo.api.{FailoverStrategy, MongoConnection, MongoDriver}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

object ThresholdMain extends IOApp {

  private val config          = ConfigFactory.load()
  private val executorService = Executors.newFixedThreadPool(8)

  private implicit val timeout: FiniteDuration      = 10 seconds
  private implicit val system: ActorSystem          = ActorSystem("System", config)
  private implicit val materializer: Materializer   = ActorMaterializer()
  private implicit val ag: AsynchronousChannelGroup = AsynchronousChannelGroup.withThreadPool(executorService)

  private val appConfig = ConfigValueProvider.provide(config)

  private val driver           = MongoDriver()
  private val mongoConfig      = appConfig.mongoConfig
  private val mongoUri         = s"mongodb://${mongoConfig.host}:${mongoConfig.port}"
  private val connection       = MongoConnection.parseURI(mongoUri).map(driver.connection)
  private val failoverStrategy = FailoverStrategy(initialDelay = 300 millis, retries = 20, delayFactor = FactorFun(1.5))
  private val packetsDb        = IO.fromFuture(IO { Future.fromTry(connection).flatMap(_.database(mongoConfig.packetsDb, failoverStrategy)) })

  private val socketHost    = "127.0.0.1"
  private val socketPort    = 9999
  private val socketAddress = new InetSocketAddress(InetAddress.getByName(socketHost), socketPort)

  override def run(args: List[String]): IO[ExitCode] = {
    val stream =
      for {
        httpClient <- BlazeClientBuilder[IO](implicitly[ExecutionContext])
                       .withResponseHeaderTimeout(3 seconds)
                       .withIdleTimeout(3 seconds)
                       .withRequestTimeout(3 seconds)
                       .stream
        packetDb <- Stream.eval(packetsDb)

        packetTopic <- Stream.eval(Topic[IO, Option[PacketSample]](None))

        trainingAccessRateRepo = new MongoSegmentAccessRateRepository(packetDb, "training_accessRates", appConfig.accessRateDetectionConfig)
        testAccessRateRepo     = new MongoSegmentAccessRateRepository(packetDb, "test_accessRates", appConfig.accessRateDetectionConfig)
        trainingPopularityRepo = new MongoSegmentPopularityRepository(packetDb, "training_popularities", appConfig.accessRateDetectionConfig)

        detectionService = new AccessRateDetectionService(appConfig.accessRateDetectionConfig,
                                                          trainingAccessRateRepo,
                                                          trainingPopularityRepo,
                                                          testAccessRateRepo)

        _ <- Stream.eval(
              {
                val infiniteIO = List(
                  PacketStream(client[IO](socketAddress), httpClient, packetTopic, aggregateTime = appConfig.accessRateDetectionConfig.smallBucket),
                  detectionService.saveTestAccessRates(packetTopic.subscribe(Int.MaxValue).collect { case Some(pg) => pg })
                ).parSequence

                val endingIO = saveThreshold(detectionService,
                                             new DetectionScoreRepository(packetDb, "detection_score"),
                                             new DetectionScoreThresholdRepository(packetDb, "score_threshold")).compile.drain

                //App finishes when first IO finishes 
                IO.race(infiniteIO, endingIO)
              }
            )
      } yield ()

    stream.compile.drain.map(_ => ExitCode.Success)
  }

  private def saveThreshold(
      detectionService: AccessRateDetectionService,
      scoresRepository: DetectionScoreRepository,
      thresholdRepository: DetectionScoreThresholdRepository)(implicit mat: Materializer, ec: ExecutionContext, timer: Timer[IO]) = {

    val scoresStream = Stream
      .awakeEvery[IO](appConfig.accessRateDetectionConfig.smallBucketSeconds - 2 seconds)
      .evalMap[IO, (Int, Option[BadnessScores])](
        _ =>
          detectionService
            .getBadnessScore()
            .map(score => (detectionService.timeIdFromTime(ZonedDateTime.now()), score)))

    val endingScoresStream = StreamUtils
      .oneCycleStream(endAfterCycle = true, scoresStream)
      .mapAsync(1) {
        case (timeId, Some(score)) => scoresRepository.save(timeId, score)
        case (_, None)             => IO.unit
      }

    for {
      _              <- Stream.eval(scoresRepository.drop())
      _              <- endingScoresStream
      thresholdSaved <- Stream.eval(scoresRepository.getMax.flatMap(_.traverse(thresholdRepository.save)).map(_.isDefined))

    } yield thresholdSaved

  }
}
