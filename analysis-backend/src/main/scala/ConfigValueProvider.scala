import java.nio.file.{Path, Paths}

import com.typesafe.config.Config
import domain.attack.detection.AccessRateDetectionService

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Try

object ConfigValueProvider {

  def provide(config: Config): ConfigValues =
    ConfigValues(
      MongoConfig(
        config.getString("mongo.host"),
        config.getInt("mongo.port"),
        "fastnetmon",
        "attacks",
        config.getString("mongo.database"),
        config.getString("mongo.collections.packet-dumps")
      ),
      KafkaConfig(
        config.getString("stats-kafka.bootstrap-servers"),
        config.getString("stats-kafka.topic")
      ),
      AccessRateDetectionService.Config(
        trainingSpan = 10 minutes,
        bigBucket = 2 minutes,
        smallBucket = 30 second
      ),
      config.getString("socket.host"),
      config.getInt("socket.port"),
      config.getString("instance-name"),
      useRealKafka = false,
      Try(config.getString("ip-database.path")).toOption.map(Paths.get(_)),
      AuthConfig(
        readKey("private", config),
        readKey("public", config)
      )
    )

  final case class ConfigValues(
      mongoConfig: MongoConfig,
      statsKafkaConfig: KafkaConfig,
      accessRateDetectionConfig: AccessRateDetectionService.Config,
      socketHost: String,
      socketPort: Int,
      instanceName: String,
      useRealKafka: Boolean,
      ipDatabasePath: Option[Path],
      authKeys: AuthConfig
  )

  final case class ApiConfig(host: String, port: Int)

  final case class MongoConfig(
      host: String,
      port: Int,
      fastnetMonDb: String,
      fastnetmonAttackCollection: String,
      packetsDb: String,
      packetDumpCollection: String,
  )

  final case class KafkaConfig(
      bootstrapServers: String,
      statsTopic: String,
  )

  final case class AuthConfig(
      privateKeyPath: Path,
      publicKeyPath: Path
  )

  private def readKey(keyType: String, config: Config): Path =
    Try { config.getString(s"auth.$keyType-key-path") }
      .map(Paths.get(_))
      .getOrElse(Paths.get(getClass.getClassLoader.getResource(s"$keyType.der").toURI))

}
