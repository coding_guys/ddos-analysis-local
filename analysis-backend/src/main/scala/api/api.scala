package api

final case class IdResponse(id: String)

final case class AuthResponse(token: String)
