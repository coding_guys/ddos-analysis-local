package api

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import infrastructure.mongo.MongoRepository.PaginatedResponse
import io.circe.{Encoder, Json}

trait Serializers {

  import DateTimeFormatter.ISO_OFFSET_DATE_TIME

  def encodeZonedDateTime(formatter: DateTimeFormatter): Encoder[ZonedDateTime] =
    Encoder.instance(time => Json.fromString(time.format(formatter)))

  implicit def encodeZonedDateTime: Encoder[ZonedDateTime] =
    encodeZonedDateTime(ISO_OFFSET_DATE_TIME)

  implicit def paginatedResponseEncoder[T](implicit encoder: Encoder[T]): Encoder[PaginatedResponse[T]] = {
    Encoder.instance(
      paginatedResponse =>
        Json.obj(
          "pageCount" -> Json.fromInt(paginatedResponse.pageCount),
          "entities"  -> Encoder.encodeList[T](encoder)(paginatedResponse.entities)
      ))
  }

}
