package api

import akka.stream.Materializer
import cats.data.{Kleisli, OptionT}
import cats.effect._
import domain.packet.{PacketSample, PacketSampleRepository}
import domain.user.UserId
import domain.{Aggregate, FilterRange, IpResolver, LiveStatisticsService}
import fs2.Sink
import fs2.concurrent.Topic
import infrastructure.json.PacketCodecs
import infrastructure.mongo.samples.MongoPacketSampleRepository
import io.circe.syntax._
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.io._
import org.http4s.server._
import org.http4s.server.middleware.CORS
import org.http4s.server.websocket._
import org.http4s.websocket.WebSocketFrame
import org.http4s.websocket.WebSocketFrame.Text

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

class PacketEndpoint(packetTopic: Topic[IO, Option[PacketSample]], packetSampleRepository: MongoPacketSampleRepository, ipResolver: IpResolver[IO])(
    implicit timer: Timer[IO],
    cs: ContextShift[IO],
    mat: Materializer,
    ec: ExecutionContext)
    extends Serializers {

  private val authUser: Kleisli[OptionT[IO, ?], Request[IO], UserId] = Kleisli(_ => OptionT.liftF(IO(UserId("123"))))
  private val Auth: AuthMiddleware[IO, UserId]                       = AuthMiddleware(authUser)

  import PacketCodecs.Encoders._
  import PacketEndpoint._

  def service: HttpService[IO] = {
    CORS {
      Auth {
        AuthedService[UserId, IO] {
          case GET -> Root / "sample-stream" as _ =>
            val send =
              packetTopic
                .subscribe(Int.MaxValue)
                .collect { case Some(packetGroup) => packetGroup }
                .map(_.asJson)
                .map(json => Text(json.toString))

            WebSocketBuilder[IO].build(send, ignoreRead)

          case GET -> Root / "aggregate-stream" as _ =>
            val send =
              packetTopic
                .subscribe(Int.MaxValue)
                .collect { case Some(packetSample) => packetSample }
                .through(Aggregate.aggregateWithin[IO](1 second))
                .through(ipResolver.resolveGroup)
                .map(_.asJson)
                .map(json => Text(json.toString))

            WebSocketBuilder[IO].build(send, ignoreRead)

          case GET -> Root / "statistics-stream" as _ =>
            val send =
              packetTopic
                .subscribe(Int.MaxValue)
                .collect { case Some(packetSample) => packetSample }
                .through(Aggregate.aggregateWithin[IO](1 second))
                .through(LiveStatisticsService.calculateLiveStats)
                .map(_.asJson)
                .map(json => Text(json.toString))

            WebSocketBuilder[IO].build(send, ignoreRead)

          case GET -> Root / "samples"
                :? PageParam(page)
                :? EntitiesPerPageParam(entitiesPerPage)
                :? L4TypeParam(l4Type)
                :? L7TypeParam(l7Type)
                :? SourceParam(source)
                :? DestinationParam(destination)
                :? PacketSizeFromParam(packetSizeFrom)
                :? PacketSizeToParam(packetSizeTo)
                as _ =>
            val query =
              PacketSampleRepository.Query(
                page,
                entitiesPerPage,
                FilterRange.empty,
                FilterRange(packetSizeFrom, packetSizeTo),
                source,
                destination,
                l4Type,
                l7Type
              )

            for {
              packetSamples <- packetSampleRepository.get(query)
              result        <- Ok(packetSamples.asJson)
            } yield result
        }
      }
    }
  }

  private val ignoreRead = Sink[IO, WebSocketFrame](_ => IO.unit)

}

object PacketEndpoint {

  object PageParam            extends QueryParamDecoderMatcher[Int]("page")
  object EntitiesPerPageParam extends QueryParamDecoderMatcher[Int]("entitiesPerPage")
  object L4TypeParam          extends OptionalQueryParamDecoderMatcher[String]("l4Type")
  object L7TypeParam          extends OptionalQueryParamDecoderMatcher[String]("l7Type")
  object SourceParam          extends OptionalQueryParamDecoderMatcher[String]("source")
  object DestinationParam     extends OptionalQueryParamDecoderMatcher[String]("destination")
  object PacketSizeFromParam  extends OptionalQueryParamDecoderMatcher[Int]("packetSizeFrom")
  object PacketSizeToParam    extends OptionalQueryParamDecoderMatcher[Int]("packetSizeTo")

}
