package api

import cats.effect._
import domain.user.UserService
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.io._
import org.http4s.server.middleware.CORS

class AuthEndpoint(userService: UserService) {

  import AuthEndpoint._

  def service: HttpService[IO] =
    CORS(
      HttpService[IO] {
        case request @ POST -> Root / "login" =>
          for {
            request     <- request.as[AuthRequest]
            userOrError <- userService.authenticate(request.email, request.password).value
            result <- userOrError match {
                       case Right(token) => Ok(AuthResponse(token.value).asJson)
                       case Left(_)      => NotFound()
                     }
          } yield result
      }
    )

}

object AuthEndpoint {

  implicit val authRequestDecoder: EntityDecoder[IO, AuthRequest] = jsonOf[IO, AuthRequest]

  final case class AuthRequest(email: String, password: String)

}
