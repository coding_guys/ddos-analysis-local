package api

import akka.stream.Materializer
import api.AttackEndpoint.AttackJson
import cats.effect._
import domain.attack.FastnetMonAttackService
import domain.attack.detection.scores.ScoresPerAttack
import infrastructure.json.AttackCodecs
import infrastructure.mongo.detection.attacks.{ArchivedL7AttacksRepository, CurrentL7AttackRepository}
import io.circe.syntax._
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.io._
import org.http4s.server.middleware.CORS

import scala.concurrent.ExecutionContext

class AttackEndpoint(attackService: FastnetMonAttackService, currentL7: CurrentL7AttackRepository, archivedL7: ArchivedL7AttacksRepository)(
    implicit mat: Materializer,
    ec: ExecutionContext)
    extends Serializers {

  import AttackCodecs.Encoders._

  def service: HttpService[IO] =
    CORS(
      HttpService[IO] {
        case POST -> Root =>
          for {
            _      <- attackService.pushNewAttackToKafka
            result <- Ok()
          } yield result

        case GET -> Root =>
          for {
            fastnetmonAttacks <- attackService.getAttacks()
            currentL7Attack   <- currentL7.getCurrentAttack()
            arcchivedL7Attack <- archivedL7.getAll
            result <- Ok(
                       (fastnetmonAttacks.map(AttackJson.fastnetmon).map(_.asJson) ++
                         arcchivedL7Attack.map(AttackJson.archivedL7).map(_.asJson) ++
                         currentL7Attack.map(AttackJson.currentL7).map(_.asJson)) asJson)
          } yield result

      }
    )

}

object AttackEndpoint {
  import com.codingguys.ddos_detection.fastnetmon.attacks.FastnetmonAttack
  case class AttackJsonFnm(attackType: String, attack: FastnetmonAttack, finished: Boolean)
  case class AttackJsonHttp(attackType: String, attack: ScoresPerAttack, finished: Boolean)
  private object AttackJson {
    def fastnetmon(a: FastnetmonAttack) = AttackJsonFnm("fastnetmon", a, false)
    def archivedL7(a: ScoresPerAttack)  = AttackJsonHttp("http", a, false)
    def currentL7(a: ScoresPerAttack)   = AttackJsonHttp("http", a, true)
  }

}
