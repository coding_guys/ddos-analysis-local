import java.net.{InetAddress, InetSocketAddress}
import java.nio.channels.AsynchronousChannelGroup
import java.nio.file.Paths
import java.time.ZonedDateTime
import java.util.concurrent.Executors

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import api.{AttackEndpoint, AuthEndpoint, PacketEndpoint}
import cats.effect._
import cats.instances.list._
import cats.syntax.parallel._
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import domain.attack.FastnetMonAttackService
import domain.attack.detection.AccessRateDetectionService
import domain.attack.detection.scores.BadnessScores
import domain.attack.detection.scores.ScoresPerAttack.TimedScore
import domain.packet.{PacketSample, PacketSamplePersistenceService, PacketStream, PacketStreamMock}
import domain.user.UserService
import domain.{DefaultUserCreator, IpResolver, StatisticsService}
import fs2._
import fs2.concurrent.Topic
import fs2.io.tcp.client
import infrastructure.auth.JWTTokenProvider
import infrastructure.auth.KeyReaders.{PrivateKeyReader, PublicKeyReader}
import infrastructure.inmem.InMemGeoLocationRepository
import infrastructure.kafka.{KafkaAttackProducer, KafkaStatsProducer}
import infrastructure.mongo.detection.attacks.{ArchivedL7AttacksRepository, CurrentL7AttackRepository}
import infrastructure.mongo.detection.score.DetectionScoreThresholdRepository
import infrastructure.mongo.detection.{MongoSegmentAccessRateRepository, MongoSegmentPopularityRepository}
import infrastructure.mongo.fastnetmon.MongoAttacksRepository
import infrastructure.mongo.samples.MongoPacketSampleRepository
import infrastructure.mongo.statistics.MongoStatisticsRepository
import infrastructure.mongo.user.MongoUserRepository
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.server.blaze.BlazeBuilder
import reactivemongo.api.FailoverStrategy.FactorFun
import reactivemongo.api.{FailoverStrategy, MongoConnection, MongoDriver}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.{higherKinds, postfixOps}

object Main extends IOApp with StrictLogging {

  private val config          = ConfigFactory.load()
  private val executorService = Executors.newFixedThreadPool(8)

  private val USE_MOCK = false
//  private val USE_THRESH = true

  private implicit val timeout: FiniteDuration            = 10 seconds
  private implicit val system: ActorSystem                = ActorSystem("System", config)
  private implicit val mat: Materializer                  = ActorMaterializer()
  private implicit val ag: AsynchronousChannelGroup       = AsynchronousChannelGroup.withThreadPool(executorService)
  private implicit val executionContext: ExecutionContext = system.dispatcher

  private val appConfig = ConfigValueProvider.provide(config)

  private val statsKafkaConfig = appConfig.statsKafkaConfig

  private val driver           = MongoDriver()
  private val mongoConfig      = appConfig.mongoConfig
  private val mongoUri         = s"mongodb://${mongoConfig.host}:${mongoConfig.port}"
  private val connection       = MongoConnection.parseURI(mongoUri).map(driver.connection)
  private val failoverStrategy = FailoverStrategy(initialDelay = 300 millis, retries = 20, delayFactor = FactorFun(1.5))
  private val packetsDb        = IO.fromFuture(IO { Future.fromTry(connection).flatMap(_.database(mongoConfig.packetsDb, failoverStrategy)) })
  private val fastnetmonDb     = IO.fromFuture(IO { Future.fromTry(connection).flatMap(_.database(mongoConfig.fastnetMonDb, failoverStrategy)) })

  private val publicKey  = PublicKeyReader.get(appConfig.authKeys.publicKeyPath)
  private val privateKey = PrivateKeyReader.get(appConfig.authKeys.privateKeyPath)

  private val socketAddress = new InetSocketAddress(InetAddress.getByName(appConfig.socketHost), appConfig.socketPort)

  private val ipDatabasePath = appConfig.ipDatabasePath.getOrElse {
    Paths.get(getClass.getResource("sample.csv").toURI)
  }

  override def run(args: List[String]): IO[ExitCode] = {
    val stream =
      for {
        httpClient <- BlazeClientBuilder[IO](executionContext)
                       .withResponseHeaderTimeout(3 seconds)
                       .withIdleTimeout(3 seconds)
                       .withRequestTimeout(3 seconds)
                       .stream
        packetDb     <- Stream.eval(packetsDb)
        fastnetmonDb <- Stream.eval(fastnetmonDb)

        packetTopic <- Stream.eval(Topic[IO, Option[PacketSample]](None))

        tokenProvider            = new JWTTokenProvider(publicKey, privateKey)
        userRepository           = new MongoUserRepository("users", packetDb)
        packetSampleRepository   = new MongoPacketSampleRepository("packet-samples", packetDb, expireAfterSeconds = 3600)
        statisticsRepository     = new MongoStatisticsRepository("statistics", packetDb)
        attacksRepository        = new MongoAttacksRepository[IO]("attacks", fastnetmonDb)
        userService              = new UserService(userRepository, tokenProvider)
        defaultUserCreator       = new DefaultUserCreator(userService)
        samplePersistenceService = new PacketSamplePersistenceService(packetSampleRepository)
        statsProducer            = new KafkaStatsProducer(statsKafkaConfig.statsTopic, statsKafkaConfig.bootstrapServers, appConfig.useRealKafka)
        attackProducer           = new KafkaAttackProducer("attacks", statsKafkaConfig.bootstrapServers, appConfig.instanceName, appConfig.useRealKafka)
        statisticsService        = new StatisticsService(statisticsRepository, statsProducer, appConfig.instanceName)

        attackService = new FastnetMonAttackService(attacksRepository, attackProducer)

        geoLocationRepository  = new InMemGeoLocationRepository()
        thresholdRepository    = new DetectionScoreThresholdRepository(packetDb, "score_threshold")
        trainingAccessRateRepo = new MongoSegmentAccessRateRepository(packetDb, "training_accessRates", appConfig.accessRateDetectionConfig)
        testAccessRateRepo     = new MongoSegmentAccessRateRepository(packetDb, "test_accessRates", appConfig.accessRateDetectionConfig)
        trainingPopularityRepo = new MongoSegmentPopularityRepository(packetDb, "training_popularities", appConfig.accessRateDetectionConfig)
        ipResolver             = new IpResolver[IO](geoLocationRepository, httpClient)

        currentL7AttackRepo  = new CurrentL7AttackRepository
        archivedL7AttackRepo = new ArchivedL7AttacksRepository(packetDb, "archivedl7attacks")
        detectionService = new AccessRateDetectionService(appConfig.accessRateDetectionConfig,
                                                          trainingAccessRateRepo,
                                                          trainingPopularityRepo,
                                                          testAccessRateRepo)

        _ <- Stream.eval(IO(geoLocationRepository.load(ipDatabasePath)))

        _ <- Stream.eval(defaultUserCreator.create())

        threshold <- Stream.eval(thresholdRepository.getThreshold)

        _ <- Stream.eval(
              {
                List(
                  if (USE_MOCK) PacketStreamMock(packetTopic)
                  else
                    PacketStream(client[IO](socketAddress), httpClient, packetTopic, aggregateTime = appConfig.accessRateDetectionConfig.smallBucket),
                  startAPI(userService, packetSampleRepository, attackService, ipResolver, currentL7AttackRepo, archivedL7AttackRepo, packetTopic)
                ) ++
                  List(
                    statisticsService.start(_),
                    detectionService.saveTestAccessRates(_),
                    samplePersistenceService.start(_)
                  ).map(_.apply(packetTopic.subscribe(Int.MaxValue).collect { case Some(packetGroup) => packetGroup }))
                    .+:(saveAndPrintL7DetectionResult(detectionService, archivedL7AttackRepo, currentL7AttackRepo, threshold).compile.drain)
              }.parSequence
            )

      } yield ()

    stream.compile.drain.map(_ => ExitCode.Success)
  }

  private def startAPI(userService: UserService,
                       packetSampleRepository: MongoPacketSampleRepository,
                       attacksService: FastnetMonAttackService,
                       ipResolver: IpResolver[IO],
                       currentL7AttackRepository: CurrentL7AttackRepository,
                       archivedL7AttacksRepository: ArchivedL7AttacksRepository,
                       packetTopic: Topic[IO, Option[PacketSample]]): IO[Unit] =
    BlazeBuilder[IO]
      .bindHttp(8080, "0.0.0.0")
      .mountService(new AuthEndpoint(userService).service, "/auth")
      .mountService(new PacketEndpoint(packetTopic, packetSampleRepository, ipResolver).service, "/")
      .mountService(new AttackEndpoint(attacksService, currentL7AttackRepository, archivedL7AttacksRepository).service, "/attacks")
      .serve
      .compile
      .drain

  private def saveAndPrintL7DetectionResult(
      detectionService: AccessRateDetectionService,
      archived: ArchivedL7AttacksRepository,
      current: CurrentL7AttackRepository,
      thresholds: Option[BadnessScores])(implicit mat: Materializer, ec: ExecutionContext, timer: Timer[IO]): Stream[IO, Option[BadnessScores]] = {
    Stream
      .awakeEvery[IO](appConfig.accessRateDetectionConfig.smallBucketSeconds - 2 seconds)
      .evalMap[IO, Option[BadnessScores]](_ => detectionService.getBadnessScore())
      .evalMap(score => {

        val maybeDiff = for {
          s <- score
          t <- thresholds
        } yield s - t

        val result = (maybeDiff match {
          case Some(diff) if diff.greaterThanZero => //attack detected
            logger.warn("ATTACK!!!")
            println("========================================================")
            println("=================== DETECTED ATTACK! ===================")
            println("========================================================")
            current.insertScore(TimedScore(ZonedDateTime.now(), score.get))
          case Some(_) | None =>
            for {
              attack <- current.popAttack()

              _ <- attack match {
                    case Some(a) => archived.save(a)
                    case None    => IO.unit
                  }
            } yield ()
        }) map (_ => score)

        logger.warn(s"BADNESS SCORE: $score")

        println(s"$score")
        println(s"$thresholds")
        println(s"$maybeDiff")

        result
      })
  }

}
