package infrastructure

import cats.effect.{Effect, Resource}
import com.typesafe.scalalogging.StrictLogging
import fs2.io.tcp.Socket
import fs2.{text, Stream}

object PacketReader extends StrictLogging {

  def readSocket[F[_]: Effect](socketResource: Resource[F, Socket[F]]): Stream[F, String] = {
    Stream
      .resource(socketResource)
      .flatMap(_.reads(1024))
      .through(text.utf8Decode)
      .through(text.lines)
  }

}
