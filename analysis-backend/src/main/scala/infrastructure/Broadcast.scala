package infrastructure

import akka.stream.Materializer
import akka.stream.scaladsl.{BroadcastHub, Keep, MergeHub, Source => AkkaSource}
import cats.effect.IO
import fs2.Pipe
import streamz.converter._

import scala.concurrent.ExecutionContext

object Broadcast {

  def broadcast[T](implicit mat: Materializer, ec: ExecutionContext): Pipe[IO, T, T] = { stream =>
    val (sink, source) =
      MergeHub
        .source[T](perProducerBufferSize = 16)
        .toMat(BroadcastHub.sink(bufferSize = 256))(Keep.both)
        .run()

    AkkaSource.fromGraph(stream.toSource).runWith(sink)

    source.toStream()
  }

}
