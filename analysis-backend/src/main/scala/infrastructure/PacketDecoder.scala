package infrastructure

import com.typesafe.scalalogging.StrictLogging
import domain.packet._
import fs2.Pipe
import infrastructure.json.PacketCodecs
import io.circe.parser.decode

object PacketDecoder extends StrictLogging {

  import PacketCodecs.Decoders._

  def decoder[F[_]]: Pipe[F, String, PacketSample] =
    _.map { rawSample =>
      decode[PacketSample](rawSample) match {
        case Right(packet) => Some(packet)
        case Left(ex) =>
          logger.warn(s"Error while decoding packet $rawSample", ex)
          None
      }
    }.collect { case Some(packet) => packet }

}
