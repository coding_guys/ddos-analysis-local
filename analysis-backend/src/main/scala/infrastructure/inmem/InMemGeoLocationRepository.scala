package infrastructure.inmem

import java.nio.file.Path

import com.typesafe.scalalogging.StrictLogging
import domain.GeoLocationRepository
import domain.IpResolver.Coordinates

import scala.io.Source

class InMemGeoLocationRepository extends GeoLocationRepository with StrictLogging {

  import InMemGeoLocationRepository._

  private var ipRanges: List[IpNumberRange] = _

  def load(csvPath: Path): Unit = {
    ipRanges = Source
      .fromFile(csvPath.toFile)
      .getLines
      .map(_.filterNot(_ == '"'))
      .map {
        _.split(",").toList match {
          case start :: end :: country :: _ :: _ :: _ :: lat :: lng :: Nil =>
            if (country != "-") Some(IpNumberRange(start.toLong, end.toLong, Coordinates(lat.toDouble, lng.toDouble)))
            else None
          case _ => None
        }
      }
      .collect { case Some(ipNumberRange) => ipNumberRange }
      .toList

    logger.info(s"Successfully loaded database of ${ipRanges.size} addresses.")
  }

  override def get(ip: String): Option[Coordinates] = {
    ipAddressToIpNumber(ip) match {
      case Some(ipNumber) =>
        ipRanges
          .find(range => ipNumber >= range.start && ipNumber <= range.end)
          .map(_.coordinates)

      case None => None
    }
  }

}

object InMemGeoLocationRepository {

  private final case class IpNumberRange(start: Long, end: Long, coordinates: Coordinates)

  private def ipAddressToIpNumber(ip: String): Option[Long] = {
    ip.split("\\.").toList match {
      case w :: x :: y :: z :: Nil => Some(16777216 * w.toLong + 65536 * x.toLong + 256 * y.toLong + z.toLong)
      case _                       => None
    }
  }

}
