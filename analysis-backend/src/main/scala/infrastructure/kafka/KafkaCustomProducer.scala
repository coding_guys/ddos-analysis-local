package infrastructure.kafka

import java.util.Properties

import com.typesafe.scalalogging.StrictLogging
import infrastructure.util.ObjectUtils
import org.apache.kafka.clients.producer.{Callback, KafkaProducer, ProducerRecord, RecordMetadata}
import org.apache.kafka.common.serialization.Serializer

import scala.concurrent.{Future, Promise}

class KafkaCustomProducer[T](topic: String, bootstrapServers: String, keyProvider: T => String, valueSerializer: Serializer[T], useRealKafka: Boolean)
    extends StrictLogging {

  private val properties = {
    val props = new Properties()
    props.put("bootstrap.servers", bootstrapServers)
    props.put("acks", "all")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", valueSerializer.getClass.getName)
    props
  }

  private val producer = if (useRealKafka) new KafkaProducer[String, T](properties) else null

  def send(message: T): Future[Unit] =
    if (useRealKafka) {
      realSend(message)
    } else {
//      logger.debug(s"KafkaProducer for topic $topic: Mocked sending a message of class ${ObjectUtils.baseName(message)}")
      Future.unit
    }

  private def realSend(message: T): Future[Unit] = {
    val promise = Promise[Unit]
    producer
      .send(
        new ProducerRecord(topic, keyProvider(message), message),
        new Callback {
          override def onCompletion(metadata: RecordMetadata, exception: Exception): Unit =
            if (exception != null) promise.failure(exception)
            else promise.success(())
        }
      )

    promise.future
  }

}
