package infrastructure.kafka

import cats.effect.Async
import com.codingguys.ddos_detection.Stats
import domain.StatsProducer
import infrastructure.util.FutureUtils

import scala.concurrent.ExecutionContext
import scala.language.higherKinds

class KafkaStatsProducer(topic: String, bootstrapServers: String, useRealKafka: Boolean)(implicit ec: ExecutionContext)
    extends KafkaCustomProducer[Stats](topic, bootstrapServers, _.instanceName, ProtoSerializer.statsSetrializer, useRealKafka)
    with StatsProducer {

  import FutureUtils._

  override def push[F[_]: Async](stats: Stats): F[Unit] = futureToAsync[F, Unit](() => send(stats))

}
