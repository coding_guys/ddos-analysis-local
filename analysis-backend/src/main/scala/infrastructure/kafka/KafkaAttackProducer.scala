package infrastructure.kafka

import cats.effect.Async
import com.codingguys.ddos_detection.fastnetmon.attacks.{FastnetmonAttack, FastnetmonAttackWrapper}
import infrastructure.util.FutureUtils

import scala.concurrent.ExecutionContext

class KafkaAttackProducer(topic: String, bootstrapServers: String, instanceName: String, useRealKafka: Boolean)(implicit ec: ExecutionContext)
    extends KafkaCustomProducer[FastnetmonAttackWrapper](topic,
                                                         bootstrapServers,
                                                         _.instanceName,
                                                         ProtoSerializer.fastnetmonAttackSerializer,
                                                         useRealKafka) {

  import FutureUtils._

  def push[F[_]: Async](stats: FastnetmonAttack): F[Unit] = futureToAsync[F, Unit](() => send(FastnetmonAttackWrapper(stats, instanceName)))

}
