package infrastructure.kafka
import com.trueaccord.scalapb.json.JsonFormat
import java.util

import com.codingguys.ddos_detection.Stats
import com.codingguys.ddos_detection.fastnetmon.attacks.FastnetmonAttackWrapper
import com.trueaccord.scalapb.GeneratedMessage
import org.apache.kafka.common.serialization.Serializer

class ProtoSerializer[Proto <: GeneratedMessage] extends Serializer[Proto] {

  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()

  override def serialize(topic: String, data: Proto): Array[Byte] = JsonFormat.toJsonString(data).getBytes

  override def close(): Unit = ()

}
object ProtoSerializer {
  implicit val statsSetrializer: Serializer[Stats]                             = new ProtoSerializer[Stats]
  implicit val fastnetmonAttackSerializer: Serializer[FastnetmonAttackWrapper] = new ProtoSerializer[FastnetmonAttackWrapper]
}
