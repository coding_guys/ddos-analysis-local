package infrastructure.auth

import java.nio.file.{Files, Path}
import java.security.spec.{PKCS8EncodedKeySpec, X509EncodedKeySpec}
import java.security.{KeyFactory, PrivateKey, PublicKey}

object KeyReaders {

  private val keyFactory = KeyFactory.getInstance("RSA")

  case object PrivateKeyReader {

    def get(filePath: Path): PrivateKey = {
      val bytes   = readBytesFromFile(filePath)
      val keySpec = new PKCS8EncodedKeySpec(bytes)
      keyFactory.generatePrivate(keySpec)
    }
  }

  case object PublicKeyReader {

    def get(filePath: Path): PublicKey = {
      val bytes   = readBytesFromFile(filePath)
      val keySpec = new X509EncodedKeySpec(bytes)
      keyFactory.generatePublic(keySpec)
    }
  }

  private def readBytesFromFile(filePath: Path): Array[Byte] = Files.readAllBytes(filePath)

}
