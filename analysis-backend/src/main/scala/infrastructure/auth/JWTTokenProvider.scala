package infrastructure.auth

import java.security.{PrivateKey, PublicKey}
import java.time.ZonedDateTime
import java.util.UUID

import cats.effect.IO
import domain.auth.TokenProvider.TokenPayload
import domain.auth.{Token, TokenProvider}
import domain.user.{User, UserId}
import org.json4s.DefaultFormats
import pdi.jwt.{JwtAlgorithm, JwtClaim, JwtJson4s}

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Try

class JWTTokenProvider(publicKey: PublicKey, privateKey: PrivateKey) extends TokenProvider {

  private implicit val formats: DefaultFormats = DefaultFormats

  private val algorithm      = JwtAlgorithm.RS256
  private val TOKEN_DURATION = 30 days
  private val ISSUER         = "coding-guys"
  private val AUDIENCE       = "coding-guys"

  override def token(user: User): IO[Token] =
    IO {
      val issuedTime = ZonedDateTime.now.toEpochSecond
      val expiration = ZonedDateTime.now.plusSeconds(TOKEN_DURATION.toSeconds).toEpochSecond
      val claims = JwtClaim(
        issuer = Some(ISSUER),
        audience = Some(Set(AUDIENCE)),
        subject = Some(user.id.value),
        expiration = Some(expiration),
        issuedAt = Some(issuedTime),
        jwtId = Some(UUID.randomUUID().toString.filter(_ != '-'))
      )

      Token(JwtJson4s.encode(claims, privateKey, algorithm))
    }

  def validateToken(accessToken: String): Try[TokenPayload] =
    JwtJson4s.decode(accessToken, publicKey, Seq(algorithm)).map { claim =>
      if (!claim.issuer.forall(_ == ISSUER)) throw new IllegalStateException(s"Invalid issuer [${claim.issuer}]")
      if (!claim.audience.forall(_.contains(AUDIENCE))) throw new IllegalStateException(s"Invalid audience [${claim.audience}]")

      val userId = claim.subject.map(UserId).getOrElse(throw new IllegalStateException(s"Invalid subject [${claim.subject}]"))

      TokenPayload(userId)
    }

}
