package infrastructure.util

import java.util.UUID

object IdProvider {

  def nextId(): String = UUID.randomUUID().toString.filterNot(_ == '-')

}
