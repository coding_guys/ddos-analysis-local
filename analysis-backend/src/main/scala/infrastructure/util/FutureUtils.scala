package infrastructure.util

import cats.effect.Async

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object FutureUtils {

  def futureToAsync[F[_]: Async, A](f: () => Future[A])(implicit ec: ExecutionContext): F[A] =
    Async[F].async { cb =>
      f().onComplete {
        case Success(a)  => cb(Right(a))
        case Failure(ex) => cb(Left(ex))
      }
    }

}
