package infrastructure.util

import java.time.{Instant, ZoneId, ZonedDateTime}
import java.util.Date

object TimeUtils {

  private val zoneId = ZoneId.systemDefault()

  implicit def dateToZonedDateTime(date: Date): ZonedDateTime = ZonedDateTime.ofInstant(date.toInstant, zoneId)

  implicit def zonedDateTimeToDate(time: ZonedDateTime): Date = Date.from(time.toInstant)

  def zonedDateTimeFromEpochSeconds(seconds: Long): ZonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochSecond(seconds), zoneId)
  def zonedDateTimeFromMillis(millis: Long): ZonedDateTime       = ZonedDateTime.ofInstant(Instant.ofEpochMilli(millis), zoneId)
  def zonedDateTimeToEpochSeconds(zoned: ZonedDateTime): Long     = zoned.toEpochSecond
  def zonedDateTimeToEpochMillis(zoned: ZonedDateTime): Long      = zoned.toInstant.toEpochMilli

}
