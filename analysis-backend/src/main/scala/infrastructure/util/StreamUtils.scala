package infrastructure.util

import com.typesafe.scalalogging.StrictLogging
import monix.execution.atomic.AtomicInt

object StreamUtils extends StrictLogging {

  def oneCycleStream[T](endAfterCycle: Boolean, streamBeg: fs2.Stream[cats.effect.IO, (Int, T)]) = {
    if (endAfterCycle) {

      val firstTimeId = AtomicInt.apply(-2)

      streamBeg
        .takeWhile {
          case (timeId, t) =>
            if (firstTimeId.get == -2) {
              logger.info(s"Real first timeId is $timeId, skipping")
              firstTimeId.set(-1)
              true
            } else if (firstTimeId.get == -1) {
              logger.info(s"Second timeId is $timeId, keeping")
              firstTimeId.set(timeId)
              true
            } else if (firstTimeId.get == timeId) {
              logger.info(s"Found repeated timeId, $timeId -, ${firstTimeId.get} ending")
              false
            } else {
              logger.info(s"secondTimeId:${firstTimeId.get}")
              true
            }
        }
    } else
      streamBeg
  }

}
