package infrastructure.util

trait ObjectUtils {

  def baseName(any: Any): String = any.getClass.getName.split("\\.").last.split("\\$").last
}
object ObjectUtils extends ObjectUtils
