package infrastructure.util

import cats.effect.IO
import com.typesafe.scalalogging.StrictLogging
import fs2.Pipe

object StreamLogger extends StrictLogging {

  def logPipe[O]: Pipe[IO, O, O] = _.evalMap(x => IO { logger.info(x.toString); x })

  def logPipe[O](stageName: String): Pipe[IO, O, O] = _.evalMap(x => IO { logger.info(s"$stageName - $x"); x })

}
