package infrastructure.mongo.user

import java.util.UUID

import cats.effect.IO
import domain.user.{User, UserId, UserRepository}
import reactivemongo.api.DefaultDB
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.Macros.Annotations.Key
import infrastructure.mongo.MongoWRs
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext

class MongoUserRepository(collection: String, db: DefaultDB)(implicit ec: ExecutionContext) extends UserRepository {

  import MongoWRs._
  import MongoUserRepository._

  private val userCollection = db.collection[BSONCollection](collection)

  override def nextIdentity(): IO[UserId] = IO { UserId(UUID.randomUUID().toString.filterNot(_ == '-')) }

  override def add(user: User): IO[Unit] =
    IO.fromFuture {
      IO {
        userCollection
          .insert(toDTO(user))
          .map(_ => ())
      }
    }

  override def get(id: UserId): IO[Option[User]] = getBy("_id", id.value)

  override def getByEmail(email: String): IO[Option[User]] = getBy("email", email)

  private def getBy(fieldName: String, value: String): IO[Option[User]] =
    IO.fromFuture {
      IO {
        userCollection
          .find(BSONDocument(fieldName -> value))
          .one[UserDTO]
          .map(_.map(toDomain))
      }
    }

}

object MongoUserRepository {

  final case class UserDTO(
      @Key("_id") id: String,
      email: String,
      passwordHash: String
  )

  def toDTO(user: User): UserDTO =
    UserDTO(user.id.value, user.email, user.passwordHash)

  def toDomain(user: UserDTO): User =
    User(UserId(user.id), user.email, user.passwordHash)

}
