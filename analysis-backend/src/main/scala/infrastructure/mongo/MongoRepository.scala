package infrastructure.mongo

import reactivemongo.api.BSONSerializationPack.{Reader, Writer}
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.{Cursor, DefaultDB, QueryOpts}
import reactivemongo.bson.{BSONDocument, BSONValue}

import scala.concurrent.{ExecutionContext, Future}

abstract class MongoRepository(db: DefaultDB, collectionName: String)(implicit ec: ExecutionContext) {

  import MongoRepository._

  val collection: BSONCollection = db.collection[BSONCollection](collectionName)

  def save[T](entity: T)(implicit writer: Writer[T]): Future[Unit] =
    collection
      .insert(entity)
      .map(_ => ())

  def setField[T <: BSONValue](id: String, field: String, value: T, upsert: Boolean = false): Future[Unit] =
    collection
      .update(BSONDocument("_id" -> id), BSONDocument("$set" -> BSONDocument(field -> value)), upsert = upsert)
      .map(_ => ())

  def getById[K <: BSONValue, T](id: K)(implicit r: Reader[T]): Future[Option[T]] =
    getBy[K, T]("_id", id)

  def getBy[K <: BSONValue, T](fieldName: String, fieldValue: K)(implicit r: Reader[T]): Future[Option[T]] =
    collection
      .find(BSONDocument(fieldName -> fieldValue))
      .one[T]

  def paginatedQuery[T](
      page: Int,
      entitiesPerPage: Int,
      query: BSONDocument,
      sort: BSONDocument
  )(implicit r: Reader[T]): Future[PaginatedResponse[T]] = {
    for {
      pageCount <- collection.count(Some(query)).map(_.toDouble / entitiesPerPage)
      entities <- collection
                   .find(query)
                   .sort(sort)
                   .options(QueryOpts((page - 1) * entitiesPerPage))
                   .cursor[T]()
                   .collect[List](entitiesPerPage, Cursor.FailOnError[List[T]]())
    } yield PaginatedResponse(Math.ceil(pageCount).toInt, entities)
  }

}

object MongoRepository {

  final case class PaginatedResponse[T](pageCount: Int, entities: List[T]) {
    def map[A](f: T => A): PaginatedResponse[A] = PaginatedResponse(pageCount, entities.map(f))
  }

}
