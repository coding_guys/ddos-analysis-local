package infrastructure.mongo

import java.time.ZonedDateTime
import java.util.Date

import infrastructure.util.TimeUtils
import reactivemongo.bson.{BSONDateTime, BSONInteger, BSONValue}

trait BSONAble[T] {
  def toBSON(t: T): BSONValue
}

object BSONAble {

  implicit object BSONAbleInt extends BSONAble[Int] {
    def toBSON(t: Int): BSONValue = BSONInteger(t)
  }

  implicit object BSONAbleDate extends BSONAble[Date] {
    def toBSON(t: Date): BSONValue = BSONDateTime(t.getTime)
  }

  implicit object BSONAbleZonedDateTime extends BSONAble[ZonedDateTime] {
    import TimeUtils._

    def toBSON(t: ZonedDateTime): BSONValue = toBSON(zonedDateTimeToDate(t))
  }

}
