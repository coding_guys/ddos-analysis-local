package infrastructure.mongo

import com.codingguys.ddos_detection._
import com.codingguys.ddos_detection.l3.L3Stats
import com.codingguys.ddos_detection.l4.{IcmpStats, L4Stats, TcpStats, UdpStats}
import com.codingguys.ddos_detection.l7.{HttpRequestStats, HttpResponseStats, L7Stats, UnknownStats}
import domain.packet._
import infrastructure.mongo.samples.MongoPacketSampleRepository.PacketSampleDTO
import infrastructure.mongo.user.MongoUserRepository.UserDTO
import reactivemongo.bson.{BSONDocument, BSONDocumentHandler, BSONDocumentReader, BSONDocumentWriter, Macros}

object MongoWRs {

  implicit val userHandler: BSONDocumentHandler[UserDTO] = Macros.handler[UserDTO]

  implicit val packetDumpHandler: BSONDocumentHandler[PacketSampleDTO] = Macros.handler[PacketSampleDTO]

  implicit val l3Handler: BSONDocumentHandler[L3] = Macros.handler[L3]

  implicit val l4Handler: BSONDocumentReader[L4] with BSONDocumentWriter[L4] =
    new BSONDocumentReader[L4] with BSONDocumentWriter[L4] {
      import L4Type._

      override def read(bson: BSONDocument): L4 = {
        val l4Type =
          bson.getAs[String]("l4_type").get match {
            case "Icmp"    => Icmp
            case "Tcp"     => Tcp
            case "Udp"     => Udp
            case "Unknown" => Unknown
          }

        L4(
          l4Type,
          bson.getAs[Int]("icmp_type"),
          bson.getAs[Int]("icmp_code"),
          bson.getAs[Int]("src_port"),
          bson.getAs[Int]("dst_port"),
          bson.getAs[Boolean]("ack"),
          bson.getAs[Boolean]("syn"),
          bson.getAs[Boolean]("rst"),
          bson.getAs[Boolean]("fin"),
          bson.getAs[Int]("window_size")
        )
      }

      override def write(t: L4): BSONDocument = {
        val l4Type =
          t.l4_type match {
            case Icmp           => "Icmp"
            case Tcp            => "Tcp"
            case Udp            => "Udp"
            case L4Type.Unknown => "Unknown"
          }

        BSONDocument(
          "l4_type"     -> l4Type,
          "icmp_type"   -> t.icmp_type,
          "icmp_code"   -> t.icmp_code,
          "src_port"    -> t.src_port,
          "dst_port"    -> t.dst_port,
          "ack"         -> t.ack,
          "syn"         -> t.syn,
          "rst"         -> t.rst,
          "fin"         -> t.fin,
          "window_size" -> t.window_size
        )
      }

    }

  implicit val l7Handler: BSONDocumentReader[L7] with BSONDocumentWriter[L7] =
    new BSONDocumentReader[L7] with BSONDocumentWriter[L7] {
      import L7Type._

      override def read(bson: BSONDocument): L7 = {
        val packet_type = bson.getAs[String]("packet_type").get match {
          case "HttpRequest"  => HttpRequest
          case "HttpResponse" => HttpResponse
          case "Unknown"      => L7Type.Unknown
        }

        L7(
          packet_type,
          method = bson.getAs[String]("method"),
          path = bson.getAs[String]("path"),
          host = bson.getAs[String]("host"),
          status_code = bson.getAs[Int]("status_code")
        )
      }

      override def write(t: L7): BSONDocument = {
        val packet_type = t.packet_type match {
          case HttpRequest    => "HttpRequest"
          case HttpResponse   => "HttpResponse"
          case L7Type.Unknown => "Unknown"
        }

        BSONDocument(
          "packet_type" -> packet_type,
          "method"      -> t.method,
          "path"        -> t.path,
          "host"        -> t.host,
          "status_code" -> t.status_code
        )
      }
    }

  implicit val l4UnknownStatsHandler: BSONDocumentReader[l4.UnknownStats] with BSONDocumentWriter[l4.UnknownStats] =
    new BSONDocumentReader[l4.UnknownStats] with BSONDocumentWriter[l4.UnknownStats] {
      override def read(bson: BSONDocument): l4.UnknownStats = l4.UnknownStats(bson.getAs[Int]("packets").get)
      override def write(t: l4.UnknownStats): BSONDocument   = BSONDocument("packets" -> t.packets)
    }

  implicit val icmpStats: BSONDocumentHandler[IcmpStats]                            = Macros.handler[IcmpStats]
  implicit val tcpStats: BSONDocumentHandler[TcpStats]                              = Macros.handler[TcpStats]
  implicit val udpStats: BSONDocumentHandler[UdpStats]                              = Macros.handler[UdpStats]
  implicit val intLabelToAmountHandler: BSONDocumentHandler[IntLabelToAmount]       = Macros.handler[IntLabelToAmount]
  implicit val stringLabelToAmountHandler: BSONDocumentHandler[StringLabelToAmount] = Macros.handler[StringLabelToAmount]
  implicit val httpRequestStatsHandler: BSONDocumentHandler[HttpRequestStats]       = Macros.handler[HttpRequestStats]
  implicit val httpResponseStatsHandler: BSONDocumentHandler[HttpResponseStats]     = Macros.handler[HttpResponseStats]
  implicit val l7UnknownStatsHandler: BSONDocumentHandler[UnknownStats]             = Macros.handler[UnknownStats]
  implicit val l3StatsHandler: BSONDocumentHandler[L3Stats]                         = Macros.handler[L3Stats]
  implicit val l4StatsHandler: BSONDocumentHandler[L4Stats]                         = Macros.handler[L4Stats]
  implicit val l7StatsHandler: BSONDocumentHandler[L7Stats]                         = Macros.handler[L7Stats]
  implicit val statsHandler: BSONDocumentHandler[Stats]                             = Macros.handler[Stats]

}
