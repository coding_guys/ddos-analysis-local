package infrastructure.mongo.samples

import akka.stream.Materializer
import cats.effect.IO
import domain.Aggregate.PacketGroup
import domain.packet._
import infrastructure.mongo.MongoRepository.PaginatedResponse
import infrastructure.mongo.{MongoRepository, MongoUtils, MongoWRs}
import reactivemongo.api.DefaultDB
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.indexes.Index
import reactivemongo.api.indexes.IndexType.Ascending
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext

class MongoPacketSampleRepository(collectionName: String, db: DefaultDB, expireAfterSeconds: Long)(implicit ec: ExecutionContext, mat: Materializer)
    extends MongoRepository(db, collectionName)
    with PacketSampleRepository
    with PacketDumpSerialization {

  import MongoPacketSampleRepository._
  import MongoWRs._

  private val samples = {
    val col      = db.collection[BSONCollection](collectionName)
    val ttlIndex = Index(key = Seq("lastModifiedDate" -> Ascending), options = BSONDocument("expireAfterSeconds" -> expireAfterSeconds))

    col.indexesManager.ensure(ttlIndex)
    col
  }

  override val collection: BSONCollection = samples

  override def add(group: PacketGroup): IO[Unit] =
    IO.fromFuture {
      IO {
        samples
          .insert[PacketSampleDTO](ordered = true)
          .many(group.packets.map(_.toDTO))
          .map(_ => ())
      }
    }

  override def get(query: PacketSampleRepository.Query): IO[PaginatedResponse[PacketSample]] =
    IO.fromFuture {
      IO {
        paginatedQuery[PacketSampleDTO](
          query.page,
          query.entitiesPerPage,
          queryToDocument(query),
          queryToSort(query)
        ).map(_.map(_.toDomain))
      }
    }

}

object MongoPacketSampleRepository extends MongoUtils {

  final case class PacketSampleDTO(
      l3: L3,
      l4: L4,
      l7: Option[L7],
      sampling_rate: Int,
      packet_size: Int,
      timestamp: Long
  )

  private def queryToDocument(query: PacketSampleRepository.Query): BSONDocument =
    BSONDocument(
      "l3.source_addr" -> query.srcIp,
      "l3.dst_addr"    -> query.dstIp,
      "l4.l4_type"     -> query.l4Type,
      "l7.packet_type" -> query.l7Type,
      "packet_size" between query.packetSize,
      "timestamp" between query.dateRange
    )

  private def queryToSort(query: PacketSampleRepository.Query): BSONDocument = {
    val sortValue = -1
    val sortField = "timestamp"
    BSONDocument(sortField -> sortValue)
  }

}
