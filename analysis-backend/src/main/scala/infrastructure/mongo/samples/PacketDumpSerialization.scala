package infrastructure.mongo.samples

import domain.packet.PacketSample
import infrastructure.mongo.samples.MongoPacketSampleRepository.PacketSampleDTO

trait PacketDumpSerialization {

  implicit class PacketSampleToDomain(dto: PacketSampleDTO) {
    def toDomain: PacketSample = PacketSample(dto.l3, dto.l4, dto.l7, dto.sampling_rate, dto.packet_size, dto.timestamp)
  }

  implicit class PacketSampleToDTO(sample: PacketSample) {
    def toDTO: PacketSampleDTO = PacketSampleDTO(sample.l3, sample.l4, sample.l7, sample.sampling_rate, sample.packet_size, sample.timestamp)
  }

}
