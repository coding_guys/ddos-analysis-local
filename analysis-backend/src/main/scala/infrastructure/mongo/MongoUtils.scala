package infrastructure.mongo

import domain.FilterRange
import infrastructure.util.TimeUtils._
import reactivemongo.bson.BSONDocument

trait MongoUtils {

  implicit class FieldInRange(key: String) {
    def between[T: BSONAble](range: FilterRange[T]): BSONDocument = valueInBetween(key, range)

    private def valueInBetween[T](fieldName: String, range: FilterRange[T])(implicit B: BSONAble[T]): BSONDocument = {
      if (range.from.isEmpty && range.to.isEmpty) BSONDocument.empty
      else
        BSONDocument(
          range.from.map(from => BSONDocument(fieldName -> BSONDocument("$gte" -> B.toBSON(from)))).getOrElse(BSONDocument.empty),
          fieldName -> BSONDocument("$exists" -> true),
          range.to.map(to => BSONDocument(fieldName -> BSONDocument("$lte" -> B.toBSON(to)))).getOrElse(BSONDocument.empty)
        )
    }
  }

}
