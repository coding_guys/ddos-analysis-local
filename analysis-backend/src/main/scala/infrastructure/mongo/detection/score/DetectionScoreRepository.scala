package infrastructure.mongo.detection.score

import cats.effect.IO
import domain.attack.detection.scores.BadnessScores
import reactivemongo.api.DefaultDB
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.Macros.Annotations.Key
import reactivemongo.bson.{BSONDocument, BSONDocumentHandler, Macros}

import scala.concurrent.{ExecutionContext, Future}
class DetectionScoreRepository(db: DefaultDB, collectionName: String)(implicit ec: ExecutionContext) {
  import DetectionScoreRepository._
  private val collection = db.collection[BSONCollection](collectionName)

  def save(timeId: Int, thresholds: BadnessScores): IO[Unit] =
    IO.fromFuture(IO {
        collection.update(BSONDocument("_id" -> timeId.toString), BadnessScoresDTO(timeId, thresholds), upsert = true)
      })
      .map(_ => Unit)

  def drop(): IO[Unit] = IO.fromFuture(IO { collection.drop(failIfNotFound = false).map(_ => ()) })

  def getMax: IO[Option[BadnessScores]] = getAllDtos.map {
    case Nil => None
    case list =>
      Some(
        BadnessScores(
          list.map(_.squareDiffSum).max,
          list.map(_.diffSum).max,
          list.map(_.squareDiffAvg).max,
          list.map(_.diffAvg).max
        ))
  }

  def getAll: IO[List[(Int, BadnessScores)]] = getAllDtos.map(_.map(dto => (dto.timeId, dto.scores)))

  private def getAllDtos: IO[List[BadnessScoresDTO]] =
    IO.fromFuture(IO {
      collection
        .find(BSONDocument.empty)
        .cursor[BadnessScoresDTO]()
        .collect[List]()
    })
}

object DetectionScoreRepository {

  private[score] case class BadnessScoresDTO(
      @Key("_id") id: String,
      timeId: Int,
      squareDiffSum: Double,
      diffSum: Double,
      squareDiffAvg: Double,
      diffAvg: Double
  ) {
    def scores = BadnessScores(squareDiffSum, diffSum, squareDiffAvg, diffAvg)
  }

  object BadnessScoresDTO {
    def apply(
        timeId: Int,
        scores: BadnessScores
    ): BadnessScoresDTO =
      BadnessScoresDTO(timeId.toString, timeId, scores.squareDiffSum, scores.diffSum, scores.squareDiffAvg, scores.diffAvg)
  }

  private[score] implicit val scoresHandler: BSONDocumentHandler[BadnessScoresDTO] = Macros.handler[BadnessScoresDTO]

}
