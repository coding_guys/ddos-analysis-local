package infrastructure.mongo.detection.score

import cats.effect.IO
import domain.attack.detection.scores.BadnessScores
import reactivemongo.api.DefaultDB
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext
class DetectionScoreThresholdRepository(db: DefaultDB, collectionName: String)(implicit ec: ExecutionContext) {
  import DetectionScoreRepository._
  private val collection = db.collection[BSONCollection](collectionName)

  def save(thresholds: BadnessScores): IO[Unit] =
    IO.fromFuture(IO {
        collection.update(BSONDocument("_id" -> "1"), BadnessScoresDTO(1, thresholds), upsert = true)
      })
      .map(_ => Unit)

  def getThreshold: IO[Option[BadnessScores]] =
    IO.fromFuture(IO {
      collection
        .find(BSONDocument.empty)
        .one[BadnessScoresDTO]
        .map(_.map(_.scores))
    })
}
