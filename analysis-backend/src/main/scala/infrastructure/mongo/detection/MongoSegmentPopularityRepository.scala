package infrastructure.mongo.detection

import cats.effect.IO
import domain.attack.detection.AccessRateDetectionService.{SegmentPopularity, TimeBucket}
import domain.attack.detection.{AccessRateDetectionService, SegmentPopularityBucketRepository}
import reactivemongo.api.DefaultDB
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.Macros.Annotations.Key
import reactivemongo.bson.{BSONArray, BSONDocument, BSONDocumentHandler, Macros}

import scala.concurrent.ExecutionContext
class MongoSegmentPopularityRepository(db: DefaultDB, collection: String, config: AccessRateDetectionService.Config)(implicit ec: ExecutionContext)
    extends SegmentPopularityBucketRepository {
  import MongoSegmentPopularityRepository._

  private val attacks = db.collection[BSONCollection](collection)

  override def save(timeBucket: AccessRateDetectionService.TimeBucket[AccessRateDetectionService.SegmentPopularity]): IO[Unit] =
    IO.fromFuture(IO {
        attacks.update(BSONDocument("_id" -> timeBucket.timeId.toString), SegmentPopularityDto.toDto(timeBucket), upsert = true)
      })
      .map(_ => Unit)

  override def get(timeId: Int): IO[Option[TimeBucket[SegmentPopularity]]] =
    IO.fromFuture(IO { attacks.find(BSONDocument("_id" -> timeId.toString)).one[SegmentPopularityDto] }).map(_.map(SegmentPopularityDto.toDomain))
  override def getAll: IO[List[AccessRateDetectionService.TimeBucket[AccessRateDetectionService.SegmentPopularity]]] =
    IO.fromFuture(IO {
        attacks
          .find(BSONDocument.empty)
          .cursor[SegmentPopularityDto]()
          .collect[List]()
      })
      .map(_.map(SegmentPopularityDto.toDomain))
}

object MongoSegmentPopularityRepository {

  private[detection] case class SegmentPopularityDto(
      @Key("_id") timeId: String,
      popularitys: List[SegmentPopularity]
  )

  object SegmentPopularityDto {
    def toDomain(dto: SegmentPopularityDto)          = TimeBucket(dto.timeId.toInt, dto.popularitys)
    def toDto(domain: TimeBucket[SegmentPopularity]) = SegmentPopularityDto(domain.timeId.toString, domain.bucket)
  }

  implicit val segmentPopularityHandler: BSONDocumentHandler[SegmentPopularity]       = Macros.handler[SegmentPopularity]
  implicit val segmentPopularityDtoHandler: BSONDocumentHandler[SegmentPopularityDto] = Macros.handler[SegmentPopularityDto]

}
