package infrastructure.mongo.detection
import cats.effect.IO
import domain.attack.detection.AccessRateDetectionService.{SegmentAccessRate, TimeBucket}
import domain.attack.detection.{AccessRateDetectionService, SegmentAccessRateBucketRepository}
import reactivemongo.api.DefaultDB
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.{BSONArray, BSONDocument, BSONDocumentHandler, BSONObjectID, Macros}
import reactivemongo.bson.Macros.Annotations.Key

import scala.concurrent.ExecutionContext
class MongoSegmentAccessRateRepository(db: DefaultDB, collection: String, config: AccessRateDetectionService.Config)(implicit ec: ExecutionContext)
    extends SegmentAccessRateBucketRepository {
  import MongoSegmentAccessRateRepository._

  private val attacks = db.collection[BSONCollection](collection)

  override def save(timeBucket: AccessRateDetectionService.TimeBucket[AccessRateDetectionService.SegmentAccessRate]): IO[Unit] =
    IO.fromFuture(IO {
        println(s"SAVING BUCKET $timeBucket")
        attacks.update(BSONDocument("_id" -> timeBucket.timeId.toString), SegmentAccessRateDto.toDto(timeBucket), upsert = true)
      })
      .map(_ => Unit)
  override def getXPrevious(x: Int, timeId: Int): IO[List[AccessRateDetectionService.TimeBucket[AccessRateDetectionService.SegmentAccessRate]]] =
    IO.fromFuture(IO {
        val prevId = (config.smallBucketsInTrainingSpan + timeId - x) % config.smallBucketsInTrainingSpan
        val query =
          if (prevId <= timeId) {
            BSONDocument(
              "$and" -> BSONArray(
                BSONDocument("timeId" -> BSONDocument("$lte" -> timeId)),
                BSONDocument("timeId" -> BSONDocument("$gte" -> prevId))
              )
            )
          } else {
            BSONDocument(
              "$or" -> BSONArray(
                BSONDocument("timeId" -> BSONDocument("$gte" -> timeId)),
                BSONDocument("timeId" -> BSONDocument("$lte" -> prevId))
              )
            )

          }
        attacks
          .find(query)
          .cursor[SegmentAccessRateDto]()
          .collect[List]()
      })
      .map(_.map(SegmentAccessRateDto.toDomain))

  override def getAll: IO[List[AccessRateDetectionService.TimeBucket[AccessRateDetectionService.SegmentAccessRate]]] =
    IO.fromFuture(IO {
        attacks
          .find(BSONDocument.empty)
          .cursor[SegmentAccessRateDto]()
          .collect[List]()
      })
      .map(_.map(SegmentAccessRateDto.toDomain))
}

object MongoSegmentAccessRateRepository {

  private[detection] case class SegmentAccessRateDto(
      @Key("_id") id: String,
      timeId: Int,
      accessRates: List[SegmentAccessRate]
  )

  object SegmentAccessRateDto {
    def toDomain(dto: SegmentAccessRateDto)          = TimeBucket(dto.timeId, dto.accessRates)
    def toDto(domain: TimeBucket[SegmentAccessRate]) = SegmentAccessRateDto(domain.timeId.toString, domain.timeId, domain.bucket)
  }

  implicit val segmentAccessRateHandler: BSONDocumentHandler[SegmentAccessRate]       = Macros.handler[SegmentAccessRate]
  implicit val segmentAccessRateDtoHandler: BSONDocumentHandler[SegmentAccessRateDto] = Macros.handler[SegmentAccessRateDto]

}
