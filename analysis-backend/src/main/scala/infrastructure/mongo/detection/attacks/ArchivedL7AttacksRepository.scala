package infrastructure.mongo.detection.attacks

import java.time.ZonedDateTime

import cats.effect.IO
import domain.attack.detection.scores.{BadnessScores, ScoresPerAttack}
import domain.attack.detection.scores.ScoresPerAttack.TimedScore
import infrastructure.mongo.detection.attacks.ArchivedL7AttacksRepository.ArchivedAttackDTO
import infrastructure.util.TimeUtils
import reactivemongo.api.DefaultDB
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.{BSONDocument, BSONDocumentHandler, BSONHandler, BSONLong, BSONNumberLike, Macros}
import reactivemongo.bson.Macros.Annotations.Key

import scala.concurrent.ExecutionContext
class ArchivedL7AttacksRepository(db: DefaultDB, collection: String)(implicit ec: ExecutionContext) {

  private val attacks = db.collection[BSONCollection](collection)

  def save(scores: ScoresPerAttack): IO[Unit] =
    IO.fromFuture(IO {
        attacks.insert(ArchivedAttackDTO(scores))
      })
      .map(_ => Unit)

  def getAll: IO[List[ScoresPerAttack]] =
    IO.fromFuture(IO {
        attacks
          .find(BSONDocument.empty)
          .cursor[ArchivedAttackDTO]()
          .collect[List]()
      })
      .map(_.map(_.scores))
}

object ArchivedL7AttacksRepository {
  case class ArchivedAttackDTO(
      @Key("_id") id: String,
      scores: ScoresPerAttack
  )
  object ArchivedAttackDTO {
    def apply(scores: ScoresPerAttack): ArchivedAttackDTO =
      new ArchivedAttackDTO(
        TimeUtils.zonedDateTimeToEpochMillis(scores.beginning.time).toString,
        scores
      )
    implicit val ZonedHandler: BSONHandler[BSONLong, ZonedDateTime] = new BSONHandler[BSONLong, ZonedDateTime] {
      override def read(bson: BSONLong): ZonedDateTime = TimeUtils.zonedDateTimeFromMillis(bson.value)

      override def write(t: ZonedDateTime): BSONLong =
        BSONLong(TimeUtils.zonedDateTimeToEpochMillis(t))
    }
    implicit val ScoresHandler: BSONDocumentHandler[BadnessScores]                = Macros.handler[BadnessScores]
    implicit val AlaHandler: BSONDocumentHandler[TimedScore]                      = Macros.handler[TimedScore]
    implicit val ScoresPerAttackHandler: BSONDocumentHandler[ScoresPerAttack]     = Macros.handler[ScoresPerAttack]
    implicit val ArchivedAttackDTOHandler: BSONDocumentHandler[ArchivedAttackDTO] = Macros.handler[ArchivedAttackDTO]
  }

}
