package infrastructure.mongo.detection.attacks
import java.util.concurrent.locks.{Lock, ReentrantLock}

import cats.effect.IO
import domain.attack.detection.scores.ScoresPerAttack
import domain.attack.detection.scores.ScoresPerAttack.TimedScore
class CurrentL7AttackRepository {
  //could be inegrated with mongo, right? :D
  private val lock: Lock              = new ReentrantLock()
  var scores: Option[ScoresPerAttack] = None

  def getCurrentAttack(): IO[Option[ScoresPerAttack]] = IO.pure(scores)

  def popAttack(): IO[Option[ScoresPerAttack]] =
    IO {
      val res = scores
      scores = None
      res
    }

  def insertScore(score: TimedScore): IO[Unit] = {
    IO.pure {
      lock.lock()
      if (scores.isEmpty) {
        scores = Some(ScoresPerAttack(score, score, score))
      } else if ((score.score - scores.get.peak.score  ).squareDiffAvg>0) {
        scores = Some(scores.get.copy(peak = score, last = score))
      } else {
        scores = Some(scores.get.copy(last = score))
      }
      lock.unlock()
    }
  }
}
