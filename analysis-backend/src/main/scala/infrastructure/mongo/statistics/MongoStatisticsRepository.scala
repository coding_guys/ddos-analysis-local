package infrastructure.mongo.statistics

import akka.stream.Materializer
import cats.effect.Async
import com.codingguys.ddos_detection.Stats
import domain.StatisticsRepository
import infrastructure.mongo.MongoWRs
import infrastructure.util.FutureUtils
import reactivemongo.api.DefaultDB
import reactivemongo.api.collections.bson.BSONCollection

import scala.concurrent.ExecutionContext

class MongoStatisticsRepository(collection: String, db: DefaultDB)(implicit ec: ExecutionContext, mat: Materializer) extends StatisticsRepository {

  import FutureUtils._
  import MongoWRs._

  private val statistics = db.collection[BSONCollection](collection)

  override def save[F[_]: Async](stats: Stats): F[Unit] =
    futureToAsync[F, Unit](
      () =>
        statistics
          .insert(stats)
          .map(_ => ())
    )

}
