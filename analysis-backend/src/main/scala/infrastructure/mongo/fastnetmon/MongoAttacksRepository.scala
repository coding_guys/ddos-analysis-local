package infrastructure.mongo.fastnetmon

import akka.stream.Materializer
import cats.effect.Async
import com.codingguys.ddos_detection.fastnetmon.attacks.{FastnetmonAttack => Attack}
import infrastructure.util.FutureUtils
import reactivemongo.api.DefaultDB
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext

class MongoAttacksRepository[F[_]: Async](collection: String, db: DefaultDB)(implicit ec: ExecutionContext, mat: Materializer) {

  import FutureUtils._
  import MongoAttackWRs._

  private val attackCollection = db.collection[BSONCollection](collection)

  def getLastAttack(): F[Option[Attack]] =
    futureToAsync[F, Option[Attack]] { () =>
      attackCollection
        .find(BSONDocument.empty)
        .sort(BSONDocument("attackTimeEpochSeconds" -> -1))
        .one[Attack]
    }

  def getAttacks(): F[List[Attack]] =
    futureToAsync[F, List[Attack]] { () =>
      attackCollection
        .find(BSONDocument.empty)
        .cursor[Attack]()
        .collect[List]()
    }

}
