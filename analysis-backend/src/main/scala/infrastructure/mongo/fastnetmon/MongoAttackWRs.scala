package infrastructure.mongo.fastnetmon

import java.text.SimpleDateFormat
import java.util.Locale

import com.codingguys.ddos_detection.fastnetmon.attacks.{FastnetmonAttack => Attack, AttackDetails}
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter, BSONObjectID}
object MongoAttackWRs {

  implicit val attackDetailsHandler: BSONDocumentReader[AttackDetails] with BSONDocumentWriter[AttackDetails] =
    new BSONDocumentReader[AttackDetails] with BSONDocumentWriter[AttackDetails] {

      override def read(bson: BSONDocument): AttackDetails = {
        AttackDetails(
          attackType = bson.getAs[String]("attack_type").get,
          initialAttackPower = bson.getAs[Int]("initial_attack_power").get,
          peakAttackPower = bson.getAs[Int]("peak_attack_power").get,
          attackDirection = bson.getAs[String]("attack_direction").get,
          attackProtocol = bson.getAs[String]("attack_protocol").get,
          totalIncomingTraffic = bson.getAs[Int]("total_incoming_traffic").get,
          totalOutgoingTraffic = bson.getAs[Int]("total_outgoing_traffic").get,
          totalIncomingPps = bson.getAs[Int]("total_incoming_pps").get,
          totalOutgoingPps = bson.getAs[Int]("total_outgoing_pps").get,
          totalIncomingFlows = bson.getAs[Int]("total_incoming_flows").get,
          totalOutgoingFlows = bson.getAs[Int]("total_outgoing_flows").get,
          averageIncomingTraffic = bson.getAs[Int]("average_incoming_traffic").get,
          averageOutgoingTraffic = bson.getAs[Int]("average_outgoing_traffic").get,
          averageIncomingPps = bson.getAs[Int]("average_incoming_pps").get,
          averageOutgoingPps = bson.getAs[Int]("average_outgoing_pps").get,
          averageIncomingFlows = bson.getAs[Int]("average_incoming_flows").get,
          averageOutgoingFlows = bson.getAs[Int]("average_outgoing_flows").get,
          incomingIpFragmentedTraffic = bson.getAs[Int]("incoming_ip_fragmented_traffic").get,
          outgoingIpFragmentedTraffic = bson.getAs[Int]("outgoing_ip_fragmented_traffic").get,
          incomingIpFragmentedPps = bson.getAs[Int]("incoming_ip_fragmented_pps").get,
          outgoingIpFragmentedPps = bson.getAs[Int]("outgoing_ip_fragmented_pps").get,
          incomingTcpTraffic = bson.getAs[Int]("incoming_tcp_traffic").get,
          outgoingTcpTraffic = bson.getAs[Int]("outgoing_tcp_traffic").get,
          incomingTcpPps = bson.getAs[Int]("incoming_tcp_pps").get,
          outgoingTcpPps = bson.getAs[Int]("outgoing_tcp_pps").get,
          incomingSynTcpTraffic = bson.getAs[Int]("incoming_syn_tcp_traffic").get,
          outgoingSynTcpTraffic = bson.getAs[Int]("outgoing_syn_tcp_traffic").get,
          incomingSynTcpPps = bson.getAs[Int]("incoming_syn_tcp_pps").get,
          outgoingSynTcpPps = bson.getAs[Int]("outgoing_syn_tcp_pps").get,
          incomingUdptraffic = bson.getAs[Int]("incoming_udp_traffic").get,
          outgoingUdpTraffic = bson.getAs[Int]("outgoing_udp_traffic").get,
          incomingUdpPps = bson.getAs[Int]("incoming_udp_pps").get,
          outgoingUdpPps = bson.getAs[Int]("outgoing_udp_pps").get,
          incomingIcmpTraffic = bson.getAs[Int]("incoming_icmp_traffic").get,
          outgoingIcmpTraffic = bson.getAs[Int]("outgoing_icmp_traffic").get,
          incomingIcmpPps = bson.getAs[Int]("incoming_icmp_pps").get,
          outgoingIcmpPps = bson.getAs[Int]("outgoing_icmp_pps").get,
        )

      }

      override def write(t: AttackDetails): BSONDocument = {
        BSONDocument(
          "attack_type"                    -> t.attackType,
          "initial_attack_power"           -> t.initialAttackPower,
          "peak_attack_power"              -> t.peakAttackPower,
          "attack_direction"               -> t.attackDirection,
          "attack_protocol"                -> t.attackProtocol,
          "total_incoming_traffic"         -> t.totalIncomingTraffic,
          "total_outgoing_traffic"         -> t.totalOutgoingTraffic,
          "total_incoming_pps"             -> t.totalIncomingPps,
          "total_outgoing_pps"             -> t.totalOutgoingPps,
          "total_incoming_flows"           -> t.totalIncomingFlows,
          "total_outgoing_flows"           -> t.totalOutgoingFlows,
          "average_incoming_traffic"       -> t.averageIncomingTraffic,
          "average_outgoing_traffic"       -> t.averageOutgoingTraffic,
          "average_incoming_pps"           -> t.averageIncomingPps,
          "average_outgoing_pps"           -> t.averageOutgoingPps,
          "average_incoming_flows"         -> t.averageIncomingFlows,
          "average_outgoing_flows"         -> t.averageOutgoingFlows,
          "incoming_ip_fragmented_traffic" -> t.incomingIpFragmentedTraffic,
          "outgoing_ip_fragmented_traffic" -> t.outgoingIpFragmentedTraffic,
          "incoming_ip_fragmented_pps"     -> t.incomingIpFragmentedPps,
          "outgoing_ip_fragmented_pps"     -> t.outgoingIpFragmentedPps,
          "incoming_tcp_traffic"           -> t.incomingTcpTraffic,
          "outgoing_tcp_traffic"           -> t.outgoingTcpTraffic,
          "incoming_tcp_pps"               -> t.incomingTcpPps,
          "outgoing_tcp_pps"               -> t.outgoingTcpPps,
          "incoming_syn_tcp_traffic"       -> t.incomingSynTcpTraffic,
          "outgoing_syn_tcp_traffic"       -> t.outgoingSynTcpTraffic,
          "incoming_syn_tcp_pps"           -> t.incomingSynTcpPps,
          "outgoing_syn_tcp_pps"           -> t.outgoingSynTcpPps,
          "incoming_udp_traffic"           -> t.incomingUdptraffic,
          "outgoing_udp_traffic"           -> t.outgoingUdpTraffic,
          "incoming_udp_pps"               -> t.incomingUdpPps,
          "outgoing_udp_pps"               -> t.outgoingUdpPps,
          "incoming_icmp_traffic"          -> t.incomingIcmpTraffic,
          "outgoing_icmp_traffic"          -> t.outgoingIcmpTraffic,
          "incoming_icmp_pps"              -> t.incomingIcmpPps,
          "outgoing_icmp_pps"              -> t.outgoingIcmpPps,
        )
      }
    }

  implicit val attackHandler: BSONDocumentReader[Attack] =
    (bson: BSONDocument) => {
      val attackName = bson.elements.filterNot(_.name == "_id").head.name
      val attack     = bson.getAs[BSONDocument](attackName).get

      Attack(
        bson.getAs[BSONObjectID]("_id").get.stringify,
        attack.getAs[String]("ip").get,
        attack.getAs[AttackDetails]("attack_details").get,
        secondsFromAttacklName(attackName)
      )

    }

  def secondsFromAttacklName(attackName: String) = {

    val string = attackName.split("_").takeRight(4).toList.reduce(_ + "-" + _)

    val format = new SimpleDateFormat("dd-MM-yy-HH:mm:ss", Locale.ENGLISH)
    val date   = format.parse(string)
    date.toInstant.getEpochSecond
  }

}
