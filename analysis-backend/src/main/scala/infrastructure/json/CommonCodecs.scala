package infrastructure.json

import java.time.ZonedDateTime

import infrastructure.util.TimeUtils.zonedDateTimeToDate
import io.circe._

object CommonCodecs {

  object Encoders {

    implicit val zonedDateTimeEncoder: Encoder[ZonedDateTime] = (time: ZonedDateTime) => Json.fromLong(zonedDateTimeToDate(time).getTime)

  }

}
