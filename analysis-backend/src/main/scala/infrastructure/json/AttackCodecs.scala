package infrastructure.json

import api.AttackEndpoint.{AttackJsonFnm, AttackJsonHttp}
import com.codingguys.ddos_detection.fastnetmon.attacks.{AttackDetails, FastnetmonAttack}
import domain.attack.detection.scores.ScoresPerAttack.TimedScore
import domain.attack.detection.scores.{BadnessScores, ScoresPerAttack}
import io.circe._
import io.circe.generic.semiauto._

object AttackCodecs {

  object Encoders {

    implicit val badnessScoresEncoder: Encoder[BadnessScores] = deriveEncoder[BadnessScores]
    implicit val timedScoreEncoder: Encoder[TimedScore]       = deriveEncoder[TimedScore]
    implicit val scoresEncoder: Encoder[ScoresPerAttack]      = deriveEncoder[ScoresPerAttack]

    implicit val attackDetails: Encoder[AttackDetails] = deriveEncoder[AttackDetails]

    implicit val fastnetmonAttackEncoder: Encoder[FastnetmonAttack] = deriveEncoder[FastnetmonAttack]

    implicit val fnmattackJsonEncoder: Encoder[AttackJsonFnm]   = deriveEncoder[AttackJsonFnm]
    implicit val httpattackJsonEncoder: Encoder[AttackJsonHttp] = deriveEncoder[AttackJsonHttp]

  }

}
