package infrastructure.json

import cats.implicits._
import domain.Aggregate.PacketGroup
import domain.IpResolver.{Coordinates, LocalizedGroup}
import domain.LiveStatisticsService.LiveStatistics
import domain.packet._
import io.circe._
import io.circe.generic.semiauto._

import scala.util.Try

object PacketCodecs {

  object Decoders {

    implicit val l4TypeDecoder: Decoder[L4Type] =
      (c: HCursor) => {
        import L4Type._

        Try {
          c.value.asString.get match {
            case "Icmp"    => Icmp
            case "Tcp"     => Tcp
            case "Udp"     => Udp
            case "Unknown" => Unknown
          }
        }.toEither.leftMap(_ => DecodingFailure(s"Unknown L4Type: ${c.value}", List.empty))
      }

    implicit val l7TypeDecoder: Decoder[L7Type] =
      (c: HCursor) => {
        import L7Type._

        Try {
          c.value.asString.get match {
            case "HttpRequest"  => HttpRequest
            case "HttpResponse" => HttpResponse
            case "Unknown"      => Unknown
          }
        }.toEither.leftMap(_ => DecodingFailure(s"Unknown L7Type: ${c.value}", List.empty))
      }

    implicit val l3Decoder: Decoder[L3]               = deriveDecoder[L3]
    implicit val l4Decoder: Decoder[L4]               = deriveDecoder[L4]
    implicit val l7Decoder: Decoder[L7]               = deriveDecoder[L7]
    implicit val packetDecoder: Decoder[PacketSample] = deriveDecoder[PacketSample]
  }

  object Encoders {
    
    import CommonCodecs.Encoders._

    implicit val l4TypeEncoder: Encoder[L4Type] =
      (a: L4Type) => {
        import L4Type._

        Json.fromString {
          a match {
            case Icmp    => "Icmp"
            case Tcp     => "Tcp"
            case Udp     => "Udp"
            case Unknown => "Unknown"
          }
        }
      }

    implicit val l7TypeEncoder: Encoder[L7Type] =
      (a: L7Type) => {
        import L7Type._

        Json.fromString {
          a match {
            case HttpRequest  => "HttpRequest"
            case HttpResponse => "HttpResponse"
            case Unknown      => "Unknown"
          }
        }
      }

    implicit val l3Encoder: Encoder[L3]               = deriveEncoder[L3]
    implicit val l4Encoder: Encoder[L4]               = deriveEncoder[L4]
    implicit val l7Encoder: Encoder[L7]               = deriveEncoder[L7]
    implicit val packetEncoder: Encoder[PacketSample] = deriveEncoder[PacketSample]

    implicit val liveStatisticsEncoder: Encoder[LiveStatistics] = deriveEncoder[LiveStatistics]
    implicit val packetGroupEncoder: Encoder[PacketGroup]       = deriveEncoder[PacketGroup]
    implicit val coordinatesEncoder: Encoder[Coordinates]       = deriveEncoder[Coordinates]
    implicit val localizedGroupEncoder: Encoder[LocalizedGroup] = deriveEncoder[LocalizedGroup]
  }

}
