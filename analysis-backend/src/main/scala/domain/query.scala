package domain

final case class FilterRange[T](from: Option[T], to: Option[T])

object FilterRange {
  def empty[T]: FilterRange[T] = new FilterRange[T](None, None)
}
