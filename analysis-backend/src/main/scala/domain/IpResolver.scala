package domain

import cats.effect.Effect
import cats.effect.concurrent.Ref
import cats.implicits._
import com.twitter.util.LruMap
import com.typesafe.scalalogging.StrictLogging
import domain.Aggregate.PacketGroup
import fs2.{Pipe, Stream}
import io.circe.generic.auto._
import org.http4s._
import org.http4s.circe._
import org.http4s.client.Client

import scala.concurrent.ExecutionContext

class IpResolver[F[_]](geoLocationRepository: GeoLocationRepository, client: Client[F])(implicit F: Effect[F], ec: ExecutionContext)
    extends StrictLogging {

  import IpResolver._

  def resolveGroup: Pipe[F, PacketGroup, List[LocalizedGroup]] = { in =>
    for {
      cache <- Stream.eval[F, Ref[F, IpCache]](Ref.of(new LruMap[String, Coordinates](8192)))
      localizedGroups <- in.flatMap { group =>
                          Stream
                            .fromIterator(group.packets.groupBy(sample => firstThreeOctets(sample.l3.source_addr)).toIterator)
                            .flatMap {
                              case (ipPrefix, packets) =>
                                resolveIp(s"$ipPrefix.0", cache)
                                  .map(LocalizedGroup(_, PacketGroup(packets)))
                            }
                            .fold(List.empty[LocalizedGroup])((acc, group) => group :: acc)
                        }

    } yield localizedGroups
  }

  private def resolveIp(ipAddress: String, cacheRef: Ref[F, IpCache]): Stream[F, Option[Coordinates]] =
    for {
      cache <- Stream.eval(cacheRef.get)
      coordinates <- cache
                      .get(ipAddress)
                      .map(coords => Stream.emit(Some(coords)).covary[F])
                      .getOrElse {
                        Stream
                          .eval { getCoordinates(ipAddress) }
                          .evalMap[F, Option[Coordinates]] {
                            case Some(coords) =>
                              cacheRef
                                .modify { cache =>
                                  cache.put(ipAddress, coords)
                                  cache -> cache
                                }
                                .map(_ => Some(coords))

                            case None => F.pure(None)
                          }
                      }
    } yield coordinates

  private def getCoordinates(ipAddress: String): F[Option[Coordinates]] = {
    geoLocationRepository.get(ipAddress) match {
      case coords @ Some(_) => F.pure(coords)
      case None             => makeApiCall(ipAddress)
    }
  }

  private def makeApiCall(ipAddress: String): F[Option[Coordinates]] = {
    logger.debug(s"Calling geo location API for $ipAddress")

    // TODO: wait for fix: https://github.com/http4s/http4s/issues/2156 or try to use alternative API

//    Try {
//      client.getThreshold(s"http://ip-api.com/json/$ipAddress") {
//        case Status.Successful(response) =>
//          response
//            .attemptAs[Coordinates]
//            .fold(
//              failure => {
//                logger.warn(s"Could not decode response for ip $ipAddress, due to: ${failure.message}")
//                None
//              },
//              coordinates => {
//                logger.debug(s"Successfully resolved coordinates $coordinates from $ipAddress")
//                Some(coordinates)
//              }
//            )
//
//        case unexpectedResponse =>
//          logger.warn(s"Could not resolve ip $ipAddress, received ${unexpectedResponse.status.code}")
//          F.pure[Option[Coordinates]](None)
//      }
//    } getOrElse {
//      logger.error(s"Error while making a request to resolve ip $ipAddress")
//      F.pure[Option[Coordinates]](None)
//    }

//    F.pure[Option[Coordinates]](
//      if ((190 to 200).map(_.toString).exists(ipAddress.startsWith)) Some(Coordinates(12.321, 12.321))
//      else if ((1 to 9).map(_.toString).exists(ipAddress.startsWith)) Some(Coordinates(32.33, 123.33))
//      else None
//    )

    F.pure[Option[Coordinates]](None)
  }

  implicit def coordinatesDecoder: EntityDecoder[F, Coordinates] = jsonOf[F, Coordinates]

}

object IpResolver {

  type IpCache = LruMap[String, Coordinates]

  final case class Coordinates(latitude: Double, longitude: Double)

  final case class LocalizedGroup(coordinates: Option[Coordinates], group: PacketGroup)

  private final case class ApiCoordinates(lat: Double, lon: Double) {
    def toDomain: Coordinates = Coordinates(lat, lon)
  }

  private def firstThreeOctets(ip: String): String = ip.take(ip.lastIndexOf('.'))

}
