package domain

import cats.effect.Async
import com.codingguys.ddos_detection.Stats

trait StatsProducer {

  def push[F[_]: Async](stats: Stats): F[Unit]

}
