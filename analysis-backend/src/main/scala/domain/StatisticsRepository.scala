package domain

import cats.effect.Async
import com.codingguys.ddos_detection.Stats

trait StatisticsRepository {

  def save[F[_]: Async](stats: Stats): F[Unit]

}
