package domain

import domain.IpResolver.Coordinates

trait GeoLocationRepository {

  def get(ip: String): Option[Coordinates]

}
