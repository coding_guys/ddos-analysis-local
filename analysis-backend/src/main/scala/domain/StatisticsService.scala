package domain

import cats.effect.{ContextShift, IO, Timer}
import com.codingguys.ddos_detection._
import com.codingguys.ddos_detection.l3.L3Stats
import com.codingguys.ddos_detection.l4._
import com.codingguys.ddos_detection.l7.{HttpRequestStats, HttpResponseStats, L7Stats}
import com.typesafe.scalalogging.StrictLogging
import domain.Aggregate.PacketGroup
import domain.packet.L7Type.{HttpRequest, HttpResponse}
import domain.packet.{L4Type, L7Type, PacketSample}
import fs2.Stream
import infrastructure.util.StreamLogger

import scala.concurrent.duration._
import scala.language.postfixOps

class StatisticsService(repository: StatisticsRepository, statsProducer: StatsProducer, instanceName: String)(implicit timer: Timer[IO],
                                                                                                              cs: ContextShift[IO])
    extends StrictLogging {

  import StatisticsService._

  def start(packetStream: Stream[IO, PacketSample]): IO[Unit] =
    packetStream
      .through(Aggregate.aggregateWithin[IO](1 second))
      .through(StreamLogger.logPipe("StatisticsService"))
      .map(calculateStats(instanceName))
      .evalMap[IO, Stats](stats => repository.save[IO](stats).map(_ => stats))
      .evalMap[IO, Stats](stats => statsProducer.push[IO](stats).map(_ => stats))
      .compile
      .drain

}

object StatisticsService {

  private def l3Stats(packets: List[PacketSample]): L3Stats = {
    val l3Packets = packets.map(_.sampling_rate).sum

    val l3ProtocolStats =
      packets
        .groupBy(_.l3.ip_protocol)
        .map {
          case (protocol, samples) =>
            IntLabelToAmount(protocol, samples.size)
        }
        .toSeq

    L3Stats(l3Packets, l3ProtocolStats)
  }

  private def l4Stats(packets: List[PacketSample]): L4Stats = {
    val tcpPackets     = packets.filter(_.l4.l4_type == L4Type.Tcp)
    val udpPackets     = packets.filter(_.l4.l4_type == L4Type.Udp)
    val icmpPackets    = packets.filter(_.l4.l4_type == L4Type.Icmp)
    val unknownPackets = packets.filter(_.l4.l4_type == L4Type.Unknown)

    val tcpStats = TcpStats(
      tcpPackets.size,
      valuesToAmount(tcpPackets, _.l4.src_port),
      valuesToAmount(tcpPackets, _.l4.dst_port),
      flagSum(tcpPackets, _.l4.ack),
      flagSum(tcpPackets, _.l4.syn),
      flagSum(tcpPackets, _.l4.rst),
      flagSum(tcpPackets, _.l4.fin)
    )

    val udpStats = UdpStats(
      udpPackets.size,
      valuesToAmount(udpPackets, _.l4.src_port),
      valuesToAmount(udpPackets, _.l4.dst_port)
    )

    val icmpStats = IcmpStats(
      icmpPackets.size,
      valuesToAmount(icmpPackets, _.l4.src_port),
      valuesToAmount(icmpPackets, _.l4.dst_port)
    )

    val unknownStats = l4.UnknownStats(unknownPackets.size)

    L4Stats(tcpStats, udpStats, icmpStats, unknownStats)
  }

  private def l7Stats(packets: List[PacketSample]): L7Stats = {
    val l7Packets           = packets.filter(_.l7.isDefined)
    val httpRequestPackets  = l7Packets.filter(_.l7.get.packet_type == HttpRequest)
    val httpResponsePackets = l7Packets.filter(_.l7.get.packet_type == HttpResponse)
    val unknownPackets      = l7Packets.filter(_.l7.get.packet_type == L7Type.Unknown)

    val httpRequestStats = HttpRequestStats(
      httpRequestPackets.size,
      labelToAmount(httpRequestPackets, _.l7.get.method),
      labelToAmount(httpRequestPackets, _.l7.get.path),
      labelToAmount(httpRequestPackets, _.l7.get.host)
    )

    val httpResponseStats = HttpResponseStats(
      httpResponsePackets.size,
      valuesToAmount(httpResponsePackets, _.l7.get.status_code)
    )

    val unknownStats = l7.UnknownStats(unknownPackets.size)

    L7Stats(httpRequestStats, httpResponseStats, unknownStats)
  }

  private def flagSum(packets: List[PacketSample], flatLens: PacketSample => Option[Boolean]): Int =
    packets.map(flatLens).count(_.contains(true))

  private def valuesToAmount(packets: List[PacketSample], valueLens: PacketSample => Option[Int]): List[IntLabelToAmount] =
    packets
      .map(valueLens)
      .collect { case Some(value) => value }
      .groupBy(identity)
      .map { case (value, ps) => IntLabelToAmount(value, ps.size) }
      .toList

  private def labelToAmount(packets: List[PacketSample], labelLens: PacketSample => Option[String]): List[StringLabelToAmount] =
    packets
      .map(labelLens)
      .collect { case Some(label) => label }
      .groupBy(identity)
      .map { case (label, ls) => StringLabelToAmount(label, ls.size) }
      .toList

  def calculateStats(instanceName: String)(packetGroup: PacketGroup): Stats = {
    val packets = packetGroup.packets

    Stats(
      l3Stats(packets),
      l4Stats(packets),
      l7Stats(packets),
      instanceName
    )
  }

}
