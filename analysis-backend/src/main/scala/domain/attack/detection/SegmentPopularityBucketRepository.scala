package domain.attack.detection

import cats.effect.IO
import domain.attack.detection.AccessRateDetectionService.{SegmentPopularity, TimeBucket}

trait SegmentPopularityBucketRepository {

  def save(timeBucket: TimeBucket[SegmentPopularity]): IO[Unit]

  def get(timeId: Int): IO[Option[TimeBucket[SegmentPopularity]]]

  def getAll: IO[List[TimeBucket[SegmentPopularity]]]

}
