package domain.attack.detection

import java.time.ZonedDateTime

import cats.effect.IO.ioConcurrentEffect
import cats.effect.{ContextShift, IO, Timer}
import cats.instances.option.catsStdInstancesForOption
import cats.syntax.traverse.toTraverseOps
import com.typesafe.scalalogging.StrictLogging
import domain.Aggregate
import domain.Aggregate.PacketGroup
import domain.attack.detection.scores.BadnessScores
import domain.packet.L7Type.HttpRequest
import domain.packet.{L7, PacketSample}
import fs2.Stream
import infrastructure.util.{StreamLogger, StreamUtils, TimeUtils}
import mouse.all._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration
import scala.language.postfixOps

class AccessRateDetectionService(
    config: AccessRateDetectionService.Config,
    trainingAccessRateRepo: SegmentAccessRateBucketRepository,
    trainingSegmentPopularityRepo: SegmentPopularityBucketRepository,
    testAccessRateRepo: SegmentAccessRateBucketRepository)(implicit ec: ExecutionContext, timer: Timer[IO], contextShift: ContextShift[IO])
    extends StrictLogging {

  import AccessRateDetectionService._
  //1 -> save Training
  //2 -> computePopularities
  //3 -> getThreshold BadnessScore and compute thresholds
  //3 -> run app, saveTestAccessRates
  //4 -> getBadnessScore

  def saveTrainingAccessRates: Stream[IO, PacketSample] => IO[Unit] =
    saveAccessRates(trainingAccessRateRepo, _, endAfterCycle = true)
      .through(StreamLogger.logPipe("AccessRateDetectionService (saveTrainingAccessRates)"))
      .compile
      .drain

  def saveTestAccessRates: Stream[IO, PacketSample] => IO[Unit] =
    saveAccessRates(testAccessRateRepo, _, endAfterCycle = false)
      .through(StreamLogger.logPipe("AccessRateDetectionService (saveTestAccessRates)"))
      .compile
      .drain

  def prepareTrainingData(packetStream: Stream[IO, PacketSample], justCompute: Boolean = false): IO[Int] =
    for {
      _ <- if (justCompute) IO.unit else saveTrainingAccessRates(packetStream)
      _ <- computePopularitiesInMongo()
    } yield 1

  def getBadnessScore(currentTimeId: Int = timeIdFromTime(ZonedDateTime.now())): IO[Option[BadnessScores]] = {
    logger.debug(s"Getting badness for timeId=$currentTimeId")

    val testPopularities = testAccessRateRepo
      .getXPrevious(config.smallBucketsInBigBucket, currentTimeId) // what if its 0?
      .map(popularityFromAccessRates(0, logger.debug(_)))
      .map(TimeBucket[SegmentPopularity](currentTimeId, _))

    trainingSegmentPopularityRepo
      .get(currentTimeId)
      .flatMap(_.traverse { train =>
        testPopularities.map(test => AccessRateDetectionService.getScoreFromPopularities(train.bucket, test.bucket, logger.debug(_)))
      })
  }

  private def saveAccessRates(repo: SegmentAccessRateBucketRepository,
                              packetStream: Stream[IO, PacketSample],
                              endAfterCycle: Boolean): Stream[IO, Unit] = {

    val streamBeg = packetStream
      .through(Aggregate.aggregateWithin(config.smallBucket))
      .filter(_.nonEmpty)
      .map { packetGroup =>
        val timeId = timeIdFromTime(packetGroup.packets.head.timestamp |> TimeUtils.zonedDateTimeFromMillis)
        (timeId, packetGroup)
      }

    StreamUtils
      .oneCycleStream(endAfterCycle, streamBeg)
      .map(toTimeBucket _ tupled)
      .through(StreamLogger.logPipe("About to save train"))
      .mapAsync(4)(repo.save)
  }

  private def computePopularitiesInMongo()(implicit ec: ExecutionContext): IO[Unit] = {
    val smallTimeIds = 0 to config.smallBucketsInTrainingSpan
    Stream
      .fromIterator[IO, Int](smallTimeIds.toIterator)
      .mapAsync(4)(
        timeId =>
          trainingAccessRateRepo
            .getXPrevious(config.smallBucketsInBigBucket, timeId)
            .map(AccessRateDetectionService.popularityFromAccessRates(0.03, logger.debug(_)))
            .flatMap(
              (trainingSegmentPopularityRepo.save _).compose(TimeBucket[SegmentPopularity](timeId, _))
          )
      )
      .compile
      .drain

  }

  def timeIdFromTime(time: ZonedDateTime): Int = {
    val timeSeconds = time.toEpochSecond
    (timeSeconds % config.trainingSpanSeconds) / config.smallBucketSeconds toInt
  }

}

object AccessRateDetectionService {

  case class Config(trainingSpan: FiniteDuration, bigBucket: FiniteDuration, smallBucket: FiniteDuration) {
    val trainingSpanSeconds: Long       = trainingSpan.toSeconds
    val bigBucketSeconds: Long          = bigBucket.toSeconds
    val smallBucketSeconds: Long        = smallBucket.toSeconds
    val smallBucketsInBigBucket: Int    = bigBucketSeconds / smallBucketSeconds toInt
    val smallBucketsInTrainingSpan: Int = trainingSpanSeconds / smallBucketSeconds toInt
  }

  case class TimeBucket[T](timeId: Int, bucket: List[T])

  case class SegmentAccessRate(segmentIndex: Int, segmentName: String, accessCount: Int)
  case class SegmentPopularity(segmentIndex: Int, segmentName: String, popularityPerIndex: Double, popularity: Double)

  private def getScoreFromPopularities(trainedPopularities: List[SegmentPopularity],
                                       testPopularities: List[SegmentPopularity],
                                       log: String => Unit): BadnessScores = {
    def getIndexPopularity(index: Int, segment: String) =
      trainedPopularities
        .collectFirst {
          case SegmentPopularity(`index`, `segment`, popularityPerIndex, _) => popularityPerIndex
        }
        .getOrElse {
          log(s"Not found train popularity for index $index and segment `$segment`")
          0d
        }

    val zippedPopuls = testPopularities
      .map {
        case SegmentPopularity(segmentIndex, segmentName, popularityPerIndex, _) =>
          popularityPerIndex -> getIndexPopularity(segmentIndex, segmentName)
      }

    val squareDiffSum = zippedPopuls.map { case (x, y) => math.pow(x - y, 2) }.sum
    val diffSum       = zippedPopuls.map { case (x, y) => math.abs(x - y) }.sum
    val squareDiffAvg = squareDiffSum / testPopularities.size
    val avgDiff       = diffSum / testPopularities.size

    BadnessScores(
      squareDiffSum,
      diffSum,
      squareDiffAvg,
      avgDiff
    )

  }

  def popularityFromAccessRates(minimalPopularity: Double, log: String => Unit): List[TimeBucket[SegmentAccessRate]] => List[SegmentPopularity] = {
    buckets =>
      val bigBucket = buckets.flatMap(_.bucket)
      val allAccessCount = bigBucket.map(_.accessCount).sum

      val unfilteredPopularities =
        bigBucket
          .groupBy(_.segmentIndex)
          .flatMap {
            case (segmentIndex, indexAccessRates) =>
              val indexAccessCount = indexAccessRates.map(_.accessCount).sum

              indexAccessRates
                .groupBy(_.segmentName)
                .map {
                  case (segmentName, rates) =>
                    val segmentAccessCount = rates.map(_.accessCount).sum.toDouble
                    SegmentPopularity(
                      segmentIndex,
                      segmentName,
                      popularityPerIndex = segmentAccessCount / indexAccessCount,
                      popularity = segmentAccessCount / allAccessCount
                    )
                }
          }

      val keeper    = unfilteredPopularities.filter(_.popularityPerIndex > minimalPopularity) 
      val thrownOut = unfilteredPopularities.filter(_.popularityPerIndex <= minimalPopularity)

      log(
        s"Kept ${keeper.size}," +
          s" ${keeper.size * 100d / unfilteredPopularities.size}%," +
          s" left out : ${thrownOut.size}," +
          s" that is ${thrownOut.size * 100d / unfilteredPopularities.size}%" +
          s" Minimal popularity = $minimalPopularity")

      keeper.toList

  }

  def toTimeBucket(smallTimeSlotId: Int, packetGroup: PacketGroup): TimeBucket[SegmentAccessRate] = {
    val requiredSegmentSize = 20

    val segmentAccssRates = packetGroup.packets
      .collect {
        case PacketSample(_, _, Some(L7(HttpRequest, _, Some(path), _, _)), _, _, _) =>
          val pathWithoutQueryParams = path.split('?').headOption.getOrElse("/")
          val segments               = pathWithoutQueryParams.split('/').filterNot(_.isEmpty)
          val emptySegments          = (segments.length until requiredSegmentSize).map(_ => "")
          (segments ++ emptySegments).zipWithIndex
      }
      .flatten
      .groupBy(identity)
      .map { case ((segment, index), ocurrences) => SegmentAccessRate(index, segment, ocurrences.size) }

    TimeBucket(smallTimeSlotId, segmentAccssRates.toList)
  }
}
