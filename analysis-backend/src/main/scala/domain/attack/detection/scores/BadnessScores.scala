package domain.attack.detection.scores

case class BadnessScores(
    squareDiffSum: Double,
    diffSum: Double,
    squareDiffAvg: Double,
    diffAvg: Double
) {
  def passesThreshold(other: BadnessScores) = {}

  def -(other: BadnessScores) =
    BadnessScores(this.squareDiffSum - other.squareDiffSum,
                  this.diffSum - other.diffSum,
                  this.squareDiffAvg - other.squareDiffAvg,
                  this.diffAvg - other.diffAvg)

  def greaterThanZero: Boolean =
    this.squareDiffSum > 0 || this.squareDiffAvg > 0 || diffAvg > 0
//  this.squareDiffSum > 0 || this.diffSum > 0 || this.squareDiffAvg > 0 || diffAvg > 0

  override def toString: String =
    Map(
      "squareDiffSum" -> squareDiffAvg,
      "diffSum"       -> diffSum,
      "squareDiffAvg" -> squareDiffAvg,
      "diffAvg"       -> diffAvg
    ).toString

}
