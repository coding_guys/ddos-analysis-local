package domain.attack.detection.scores
import java.time.ZonedDateTime

import domain.attack.detection.scores.ScoresPerAttack.TimedScore

case class ScoresPerAttack(beginning:TimedScore,peak:TimedScore,last:TimedScore)

object ScoresPerAttack{
  case class TimedScore(time:ZonedDateTime,score:BadnessScores)
}
