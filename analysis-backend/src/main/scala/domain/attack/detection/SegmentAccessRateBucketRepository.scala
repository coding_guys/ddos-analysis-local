package domain.attack.detection

import cats.effect.IO
import domain.attack.detection.AccessRateDetectionService.{SegmentAccessRate, TimeBucket}

trait SegmentAccessRateBucketRepository {

  def save(timeBucket: TimeBucket[SegmentAccessRate]): IO[Unit]

  def getXPrevious(x: Int, timeId: Int): IO[List[TimeBucket[SegmentAccessRate]]]

  def getAll: IO[List[TimeBucket[SegmentAccessRate]]]

}
