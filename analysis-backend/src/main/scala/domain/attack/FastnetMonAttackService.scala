package domain.attack

import cats.effect.IO
import com.codingguys.ddos_detection.fastnetmon.attacks.{FastnetmonAttack => Attack}
import com.typesafe.scalalogging.StrictLogging
import infrastructure.kafka.KafkaAttackProducer
import infrastructure.mongo.fastnetmon.MongoAttacksRepository

class FastnetMonAttackService(attacksRepository: MongoAttacksRepository[IO], kafkaAttackProducer: KafkaAttackProducer) extends StrictLogging {

  def pushNewAttackToKafka: IO[Unit] =
    for {
      attack <- attacksRepository.getLastAttack()
      _      <- IO { logger.info(s"Pushing attack $attack to kafka") }
      _      <- kafkaAttackProducer.push[IO](attack.get)
    } yield ()

  def getAttacks(): IO[List[Attack]] = attacksRepository.getAttacks()

}
