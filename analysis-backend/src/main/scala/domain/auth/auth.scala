package domain.auth

final case class Token(value: String)
