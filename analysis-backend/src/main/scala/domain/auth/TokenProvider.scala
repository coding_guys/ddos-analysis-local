package domain.auth

import cats.effect.IO
import domain.user.{User, UserId}

trait TokenProvider {

  def token(user: User): IO[Token]

}

object TokenProvider {

  final case class TokenPayload(userId: UserId)

}
