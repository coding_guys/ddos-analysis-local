package domain.packet

final case class PacketSample(
    l3: L3,
    l4: L4,
    l7: Option[L7],
    sampling_rate: Int,
    packet_size: Int,
    timestamp: Long
)

final case class L3(
    ip_protocol: Int,
    source_addr: String,
    dst_addr: String
)

sealed trait L4Type
object L4Type {
  case object Icmp    extends L4Type
  case object Tcp     extends L4Type
  case object Udp     extends L4Type
  case object Unknown extends L4Type
}

final case class L4(
    l4_type: L4Type,
    icmp_type: Option[Int],
    icmp_code: Option[Int],
    src_port: Option[Int],
    dst_port: Option[Int],
    ack: Option[Boolean],
    syn: Option[Boolean],
    rst: Option[Boolean],
    fin: Option[Boolean],
    window_size: Option[Int]
)

sealed trait L7Type
object L7Type {
  case object HttpRequest  extends L7Type
  case object HttpResponse extends L7Type
  case object Unknown      extends L7Type
}

final case class L7(
    packet_type: L7Type,
    method: Option[String],
    path: Option[String],
    host: Option[String],
    status_code: Option[Int]
)
