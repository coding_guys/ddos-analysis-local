package domain.packet

import cats.effect.{ContextShift, IO, Timer}
import domain.Aggregate
import domain.Aggregate.PacketGroup
import fs2.{Pipe, Stream}
import infrastructure.util.StreamLogger

import scala.concurrent.duration._
import scala.language.postfixOps

class PacketSamplePersistenceService(repository: PacketSampleRepository)(implicit timer: Timer[IO], cs: ContextShift[IO]) {

  def start(packetStream: Stream[IO, PacketSample]): IO[Unit] =
    packetStream
      .through(Aggregate.aggregateWithin[IO](1 second))
      .through(StreamLogger.logPipe("PacketSamplePersistenceService"))
      .through(persist)
      .compile
      .drain

  def persist: Pipe[IO, PacketGroup, Unit] = _.evalMap(repository.add)

}
