package domain.packet

import java.util.Date

import cats.effect.{ConcurrentEffect, IO, Timer}
import fs2.Stream
import fs2.concurrent.Topic
import infrastructure.util.StreamLogger

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Random

object PacketStreamMock {

  def apply(topic: Topic[IO, Option[PacketSample]])(implicit timer: Timer[IO], ce: ConcurrentEffect[IO]): IO[Unit] =
    Stream
      .awakeEvery[IO](3 seconds)
      .evalMap[IO, PacketSample](_ => IO { randomPacket() })
      .map(Some(_))
      .through(StreamLogger.logPipe("PacketStreamMock"))
      .to(topic.publish)
      .compile
      .drain

  private def randomPacket(): PacketSample =
    PacketSample(
      L3(
        ip_protocol = 1,
        source_addr = randomIp(),
        dst_addr = randomIp()
      ),
      L4(
        l4_type = L4Type.Tcp,
        icmp_type = Some(1),
        icmp_code = Some(1),
        src_port = Some(1234),
        dst_port = Some(1234),
        ack = Some(true),
        syn = Some(false),
        rst = Some(false),
        fin = Some(false),
        window_size = Some(3)
      ),
      Some(
        L7(
          packet_type = L7Type.HttpRequest,
          method = Some("POST"),
          path = Some("/login"),
          host = Some("localhost"),
          status_code = Some(200)
        )
      ),
      3,
      randomPacketSize(),
      new Date().getTime
    )

  private def randomIp(): String = {
    def rand256()             = Random.nextInt(255) + 1
    def isPrivate(ip: String) = ip.startsWith("240") || ip.startsWith("192") || ip.startsWith("10")

    val randomAddress = s"${rand256()}.${rand256()}.${rand256()}.${rand256()}"

    if (isPrivate(randomAddress)) randomIp()
    else {
//      randomAddress
      if (Math.random() > 0.2) "3.194.139.0"
      else "1.0.85.1"
    }
  }

  private def randomPacketSize() = Random.nextInt(1024) + 1

}
