package domain.packet

import java.time.ZonedDateTime

import cats.effect.IO
import domain.Aggregate.PacketGroup
import domain.FilterRange
import fs2.Stream
import infrastructure.mongo.MongoRepository.PaginatedResponse

trait PacketSampleRepository {

  import PacketSampleRepository._

  def add(group: PacketGroup): IO[Unit]

  def get(query: Query): IO[PaginatedResponse[PacketSample]]

}

object PacketSampleRepository {

  final case class Query(
      page: Int = 1,
      entitiesPerPage: Int = 12,
      dateRange: FilterRange[ZonedDateTime] = FilterRange.empty,
      packetSize: FilterRange[Int] = FilterRange.empty,
      srcIp: Option[String] = None,
      dstIp: Option[String] = None,
      l4Type: Option[String] = None,
      l7Type: Option[String] = None
  )

  object Query {
    val empty: Query = Query()
  }

}
