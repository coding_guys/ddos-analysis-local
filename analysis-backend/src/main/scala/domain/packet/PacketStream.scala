package domain.packet

import cats.effect.{ConcurrentEffect, IO, Resource, Timer}
import domain.Aggregate
import domain.Aggregate.PacketGroup
import fs2.concurrent.Topic
import fs2.io.tcp.Socket
import infrastructure.util.StreamLogger
import infrastructure.{PacketDecoder, PacketReader}
import org.http4s.client.Client

import scala.concurrent.duration.{FiniteDuration, _}
import scala.language.postfixOps

object PacketStream {

  def apply(socketResource: Resource[IO, Socket[IO]],
            client: Client[IO],
            packetTopic: Topic[IO, Option[PacketSample]],
            aggregateTime: FiniteDuration)(implicit timer: Timer[IO], ce: ConcurrentEffect[IO]): IO[Unit] =
    PacketReader
      .readSocket[IO](socketResource)
      .through(PacketDecoder.decoder[IO])
//      .through(StreamLogger.logPipe("PacketStream"))
      .map(Some(_))
      .to(packetTopic.publish)
      .compile
      .drain

}
