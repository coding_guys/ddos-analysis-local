package domain

import cats.effect.IO
import com.typesafe.scalalogging.StrictLogging
import domain.user.UserService

class DefaultUserCreator(userService: UserService) extends StrictLogging {

  private val EMAIL    = "test@test.com"
  private val PASSWORD = "ala123"

  def create(): IO[Unit] = {
    userService.createUser(EMAIL, PASSWORD).value map {
      case Right(_)    => logger.info("Successfully created the default user!")
      case Left(error) => logger.info(s"Did not create the default admin because of: $error")
    }
  }

}
