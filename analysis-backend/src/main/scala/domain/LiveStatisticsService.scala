package domain

import java.time.ZonedDateTime

import domain.Aggregate.PacketGroup
import fs2._

object LiveStatisticsService {

  def calculateLiveStats[F[_]]: Pipe[F, PacketGroup, LiveStatistics] = {
    _.map { group =>
      val packets     = group.packets
      val packetCount = packets.map(_.sampling_rate).sum
      val bytesSum    = packets.map(_.packet_size).sum

      LiveStatistics(packetCount, bytesSum, ZonedDateTime.now)
    }

  }

  final case class LiveStatistics(packetsPerSecond: Long, bytesPerSecond: Long, time: ZonedDateTime)

  object LiveStatistics {
    def empty = LiveStatistics(0L, 0L, ZonedDateTime.now)
  }

}
