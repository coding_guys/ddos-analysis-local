package domain.user

import cats.data.EitherT
import cats.effect.IO
import com.roundeights.hasher.Implicits._
import domain.auth.{Token, TokenProvider}

class UserService(userRepository: UserRepository, tokenProvider: TokenProvider) {

  import UserService._

  def createUser(email: String, password: String): EitherT[IO, CreateUserError, User] = {
    import CreateUserErrors._

    def validateThatDoesNotExist(userOpt: Option[User]): Either[CreateUserError, Unit] =
      Either.cond(userOpt.isEmpty, (), UserAlreadyExists)

    for {
      _      <- EitherT(userRepository.getByEmail(email) map validateThatDoesNotExist)
      userId <- EitherT.right(userRepository.nextIdentity())
      user   = User(userId, email, hashPassword(password))
      _      <- EitherT.right(userRepository.add(user))
    } yield user
  }

  def authenticate(email: String, password: String): EitherT[IO, AuthenticateUserError, Token] = {
    import AuthenticateUserErrors._

    def enforceUserExistence(userOpt: Option[User]): Either[AuthenticateUserError, User] =
      Either.cond(userOpt.isDefined, userOpt.get, UserDoesNotExist)

    def validatePassword(userPasswordHash: String): Either[AuthenticateUserError, Unit] =
      Either.cond(userPasswordHash == hashPassword(password), (), InvalidPassword)

    for {
      user  <- EitherT(userRepository.getByEmail(email) map enforceUserExistence)
      _     <- EitherT.fromEither[IO](validatePassword(user.passwordHash))
      token <- EitherT.right(tokenProvider.token(user))
    } yield token
  }

  private def hashPassword(password: String): String = password.sha256.hex

}

object UserService {

  sealed trait CreateUserError
  object CreateUserErrors {
    case object UserAlreadyExists extends CreateUserError
  }

  sealed trait AuthenticateUserError
  object AuthenticateUserErrors {
    case object UserDoesNotExist extends AuthenticateUserError
    case object InvalidPassword  extends AuthenticateUserError
  }

}
