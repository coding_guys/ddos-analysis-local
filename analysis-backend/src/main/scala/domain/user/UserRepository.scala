package domain.user

import cats.effect.IO

trait UserRepository {

  def nextIdentity(): IO[UserId]

  def add(user: User): IO[Unit]

  def get(id: UserId): IO[Option[User]]

  def getByEmail(email: String): IO[Option[User]]

}
