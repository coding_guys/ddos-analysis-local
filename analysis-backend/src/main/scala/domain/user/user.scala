package domain.user

final case class UserId(value: String)

final case class User(id: UserId, email: String, passwordHash: String)
