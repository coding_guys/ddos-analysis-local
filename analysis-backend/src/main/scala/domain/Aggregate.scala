package domain

import cats.effect.{ConcurrentEffect, Timer}
import domain.packet.PacketSample
import fs2.Pipe

import scala.concurrent.duration._
import scala.language.postfixOps

object Aggregate {

  def aggregateWithin[F[_]](time: FiniteDuration)(implicit timer: Timer[F], ce: ConcurrentEffect[F]): Pipe[F, PacketSample, PacketGroup] =
    _.groupWithin(Int.MaxValue, time).map(chunk => PacketGroup(chunk.toList))

  final case class PacketGroup(packets: List[PacketSample]) {
    def isEmpty: Boolean  = packets.isEmpty
    def nonEmpty: Boolean = packets.nonEmpty
  }

}
