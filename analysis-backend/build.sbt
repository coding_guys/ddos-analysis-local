name := "local-analysis-backend"
version := "0.3"
scalaVersion := "2.12.7"

enablePlugins(SbtNativePackager)
enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.8")

lazy val `root` =
  project
    .in(file("."))
    .settings(cancelable in Global := true)
    .settings(resolvers ++= Dependencies.resolvers)
    .settings(libraryDependencies ++= Dependencies.all)
    .settings(
      dockerBaseImage := "java:openjdk-8",
      daemonUser in Docker := "root",
      dockerRepository := Some("ddosgroup"),
      dockerExposedPorts := Seq(8080)
    )
    .settings(PB.targets in Compile := Seq(
      scalapb.gen(flatPackage = true) -> (sourceManaged in Compile).value
    ))

PB.protoSources in Compile := Seq(
  baseDirectory.value / "proto-definitions"
)
