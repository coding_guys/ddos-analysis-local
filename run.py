#!/usr/bin/python3
from argparse import ArgumentParser
from actions import *
from sys import argv
from functional import seq


def run():
    parser = ArgumentParser(epilog="for cleaning:\n {} clean".format(argv[0]))

    parser.add_argument("external_network", type=str,
                        help="network in which there is public ip of protected app e. g. 192.168.0.0/24")
    parser.add_argument("-v", "--verbose", help="should mongo saver log packets?", action="store_true")
    parser.add_argument("--pcap", help="Log pcap", action="store_true")
    parser.add_argument("--startmongo", help="Start mongo", action="store_true")
    parser.add_argument("--sflowcollector", help="Run sflow collector server", action="store_true")
    args = parser.parse_args()

    forwared_ports = []
    pcap_sflow_port = 6340
    fastnetmon_sflow_port = 6342
    collector_server_port = 9999
    collector_sflow_port = 6341

    forwared_ports.append(fastnetmon_sflow_port)

    if args.pcap:
        forwared_ports.append(pcap_sflow_port)

    if args.sflowcollector:
        forwared_ports.append(collector_sflow_port)

    forwared_ports = seq(forwared_ports).map(lambda sflow_port: "0.0.0.0/{}".format(sflow_port)).to_list()

    start_successful = \
        run_forwarding_sflowtool(forwared_ports) and \
        ((not args.startmongo) or start_local_mongo()) and \
        run_fastnetmon(fastnetmon_sflow_port, args.external_network) and \
        ((not args.pcap) or run_pcap_sflow(pcap_sflow_port)) and \
        ((not args.sflowcollector) or run_sflow_collector(collector_sflow_port, collector_server_port))

    if start_successful:
        print("Successfully started.\n")
    else:
        print("Failed to start.\n")
        success = clean()
        print("Clean {}".format("successful" if success else "failed."))


if __name__ == '__main__':
    if len(argv) > 1 and argv[1] == "clean":
        clean()
    else:
        run()
