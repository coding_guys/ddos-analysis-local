#!/bin/bash

function check_if_envs_exist {
    success=true
    
    for env_var_name in "$@"
    do
        if  declare -p ${env_var_name} &>/dev/null
        then
            echo "$env_var_name is set"
        else
            echo "$env_var_name not set"
            success=false
        fi
    done
    
    if ${success}
    then
        echo "All env variables are set."
    else
        echo "Not all env variables are set, exiting"
        exit -1
    fi
}