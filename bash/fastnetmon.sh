#!/bin/bash
source bash/utils.sh

check_if_envs_exist EXTERNAL_NETWORK DOCKER_NETWORK OVS_NETWORK ANALYSIS_MONGO_CONTAINER_NAME FASTNETMON_SLFOW_PORT

# FASTNETMON_LOG="/var/log/fastnetmon.log"
# FNM_ATTACK_LOG_DIR="/var/log/fastnetmon_attacks"
# NETWORK_LIST="/etc/networks.list"
# TMP_FASTNETMON_CONF="/etc/fastnetmon.conf"
# TMP_NOTIFY_SH="/notify.sh"

PWD=`pwd`

FASTNETMON_LOG="$PWD/tmp/log/fastnetmon.log"
FNM_ATTACK_LOG_DIR="$PWD/tmp/log/fastnetmon_attacks"
NETWORK_LIST="$PWD/tmp/networks.list"
TMP_FASTNETMON_CONF="$PWD/tmp/fastnetmon.conf"
TMP_NOTIFY_SH="$PWD/tmp/notify.sh"

rm -rf tmp
mkdir -p tmp/log

sudo touch $FASTNETMON_LOG
sudo chmod 777 $FASTNETMON_LOG

sudo mkdir  $FNM_ATTACK_LOG_DIR
sudo chmod 777 $FNM_ATTACK_LOG_DIR


echo -e "$EXTERNAL_NETWORK\n$DOCKER_NETWORK\n$OVS_NETWORK" | sudo tee $NETWORK_LIST 1>/dev/null
#sudo wget https://raw.githubusercontent.com/pavel-odintsov/fastnetmon/master/src/fastnetmon.conf -O /etc/fastnetmon.conf

sudo cp bash/fastnetmon/fastnetmon.conf $TMP_FASTNETMON_CONF
sudo cp bash/fastnetmon/notify.sh $TMP_NOTIFY_SH

sudo docker run -d  --name=fastnetmon \
  -v $FNM_ATTACK_LOG_DIR:/var/log/fastnetmon_attacks \
  -v $FASTNETMON_LOG:/var/log/fastnetmon.log \
  -v $NETWORK_LIST:/etc/networks_list \
  -v $TMP_FASTNETMON_CONF:/etc/fastnetmon.conf \
  -v $TMP_NOTIFY_SH:/notify.sh \
  -p 0.0.0.0:$FASTNETMON_SLFOW_PORT:6343/udp \
  --link $ANALYSIS_MONGO_CONTAINER_NAME\
  -t ddosgroup/fastnetmon /opt/fastnetmon/fastnetmon
