#!/bin/bash

set -e
COLLECTOR_DIR=sflow_collector
BRANCH=rust-sflow-development
git clone -b $BRANCH --single-branch https://github.com/JakDar/sflow-collector.git $COLLECTOR_DIR
cd $COLLECTOR_DIR
cargo build
cd ..
mv $COLLECTOR_DIR/target/debug/sflow-collector .
rm -rf $COLLECTOR_DIR
