#!/bin/bash

HOST="localhost"
PORT=8080

echo $@ > /tmp_atack
now=`date "+%d.%m.%Y %T"`
args=$@
echo $now >> /attack_detected
echo $args >> /attack_detected

curl -X POST http://$HOST:$PORT/attacks
