fastnetmon:
	docker exec -it fastnetmon /opt/fastnetmon/fastnetmon_client

run_mongo:
	sudo docker run -d --name=ddosmongo -p 0.0.0.0:27017:27017 mongo

mongo:
	sudo docker exec -it  ddosmongo  /usr/bin/mongo

install_sflow_collector:
	./bash/install_sflow_collector.sh

clean:
	sudo python3 run.py clean
	sudo rm nohup.out
