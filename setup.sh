#!/bin/bash

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# add yarn key
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

apt-get update && apt-get upgrade

# Make sure you are about to install from the Docker repo
# instead of the default Ubuntu 16.04 repo:
apt-cache policy docker-ce

echo "=== INSTALLING DOCKER ==="
apt-get install -y docker-ce

echo "=== INSTALLING USEFULL TOOLS ==="
apt-get install -y \
        iputils-ping \
        net-tools \
        traceroute \
        tcpdump \
        git \
        tree \
        hping3 \
        python3-pip \
        tmux

echo "=== INSTALL FRONTEND SIDE DEPENDENCIES === "
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get update && sudo apt-get install -y yarn nodejs

cd ~

echo "=== CLONING PROJECT REPOSITORY ==="
git clone https://Porcupine96@bitbucket.org/coding_guys/ddos-analysis-local.git

echo "=== CLONING LOCAL FRONTED==="
git clone https://Porcupine96@bitbucket.org/coding_guys/local-frontend.git

echo "=== PULLING DOCKER IMAGES ==="
docker pull ddosgroup/fastnetmon:latest
docker pull bkimminich/juice-shop:latest
docker pull mongo:latest

echo "=== CLONING SFLOWTOOL REPO ==="
git clone https://github.com/sflow/sflowtool.git

echo "=== BUILDING & INSTALLING SFLOWTOOL ==="
apt-get install dh-autoreconf -y
cd sflowtool
./boot.sh
./configure
make
sudo make install

echo "=== INSTALLING PYTHON DEPENDENCIES ==="
pip3 install pyfunctional


echo "=== INSTALLING RUST ==="
curl -sSf https://static.rust-lang.org/rustup.sh | sh

echo "=== INSTALLING SFLOW COLLECTOR ==="
cd ddos-analysis-local
make install_sflow_collector

echo "=== INSTALLING OPEN JDK 8 ==="
sudo apt-get install openjdk-8-jdk

echo "=== INSTALLING SBT ==="
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt-get update
sudo apt-get install sbt

echo "=== INITIALIZE SUBMODULES ==="
cd ddos-analysis-local/analysis-backend
rm -r proto-definitions
git clone https://Porcupine96@bitbucket.org/coding_guys/proto-definitions.git
cd ../..
